import { Component } from '@angular/core'
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service'
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { __messageSnackBar } from '../../utils/helpers';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ILoginModel } from 'src/app/shared/models/response';
import { saveModulos, saveToken, saveUserData } from '../../utils/storage';
import { ObvsService } from '../../shared/services/obvs.service';

@Component({
  selector: 'layout-auth',
  templateUrl: './login.component.html'
})
export class LayoutLoginComponent {
  logo           : String
  formGroup      : FormGroup
  routerAnimation: String

  constructor(
    private _authService: AuthService,
    private _matSnackBar: MatSnackBar,
    private _router     : Router,
    private _obvsService: ObvsService
  ) { 
    this.formGroup = new FormGroup({
        usename : new FormControl(null, [Validators.required]),
        password: new FormControl(null, [Validators.required])
    })
  }

  async authUser() {
    try {
      this._obvsService.toogleSpinner();
      this.formGroup.disable();
      const obj = this.formGroup.value;
      const result:ILoginModel = await this._authService.authUser(obj);
      //Validamos los errores
      if(result.Identificador == "success") {
        //guarda los datos del personal y el navbar 
        debugger;
        result.Personal.Token=result.Token
        saveToken(result.Token)
        saveUserData(result.Personal)
        saveModulos(result.Menu)
        this._router.navigate(['GestionCitas/Citas'])
      }
      __messageSnackBar(this._matSnackBar, result.Descripcion)
    } catch(err) {
      console.log(err)
      __messageSnackBar(this._matSnackBar, err.Descripcion);
    } finally {
      this._obvsService.toogleSpinner();
      this.formGroup.enable();
    }
  }
}
