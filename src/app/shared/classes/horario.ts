import { stringify } from "@angular/compiler/src/util";
import * as moment from "moment";
import { IPersonal } from "src/app/pages/citas/interface/interface";

export interface Hora {
    id: number;
    value: string;
    disabled: boolean;
}

export class Horario {

    public  generarHorasDia(horaInicio: number, horaFin: number): Hora[] {
        let horario: Hora[] = [];
        let inicioHora = horaInicio;
        let index = 0;
        while(inicioHora < horaFin) {
            let hora: string;
            let minutoInicio = 0;
            while(minutoInicio <= 45) {
                
                hora = `${inicioHora}:${this.calcularStringHora(minutoInicio)}`
                horario.push({
                    id: index,
                    value: hora,
                    disabled: false
                });
                minutoInicio = minutoInicio + 15;
                index++;
            }
            inicioHora++;
        }
        horario.push({
            id: index,
            value: `${inicioHora}:${this.calcularStringHora(0)}`,
            disabled: false
        });

        return horario;


        
    }

    public bloquearHorariosOcupados(horarioOcupado: IPersonal[],duracionServicio: number, diaActual: string): Hora[] {
        console.log('PROBANDO');
        let horarios = this.generarHorasDia(7,20);
        horarioOcupado[0].HorarioOcupado.forEach(item => {
            let horarioInicioOcupado = moment(item.FechaHora).format('HH:mm');
            let horarioFinOcupado = moment(item.FechaHora).add(item.Duracion, 'minutes').format('HH:mm');
            let valueId: number;
            for (const horario of horarios) {
                // Hora Actual
                let now = moment(moment().format('HH:mm'),'HH:mm');
                // Bloqueamos las horas ocupadas
                let horaFinTemp = moment(horario.value,'HH:mm'); // Horario Generado
                let horarioInicio = moment(horarioInicioOcupado,'HH:mm'); // Horario Inicio Ocupado
                let horaFin = moment(horarioFinOcupado,'HH:mm'); // Hora Fin Ocupado
                if((horaFinTemp >= horarioInicio && horaFinTemp <= horaFin) ) {
                    valueId = horario.id;
                    horarios.map(hor => {
                        if(hor.id === valueId) {
                            hor.disabled = true;
                        }
                        return hor;
                    })
                }

                // let nowDay = moment(moment(diaActual).format('yyyy-MM-DD'),'yyyy-MM-DD');
                let nowMoment = moment().format('yyyy-MM-DD');
                if( diaActual === nowMoment   ) {
                    if(horaFinTemp < now) {
                        valueId = horario.id;
                        horarios.map(hor => {
                            if(hor.id === valueId) {
                                hor.disabled = true;
                            }
                            return hor;
                        })
                    }
                    
                }
                

                
            }                       
        })

        // Bloqueamos los horarios que al sumar la duración del servicio esté bloqueado
        for(const horario of horarios) {
            let hora = moment(horario.value,'HH:mm');
            let horaFinFormat = hora.add(duracionServicio,'minutes').format('HH:mm');
            let horaFin = moment(horaFinFormat,'HH:mm');
            const buscar = this.obtenerHorarioRango(horario.value, horaFinFormat, horarios);
            if(buscar.length > 0) {
                horarios.map(hor => {
                    if(hor.id === horario.id) {
                        hor.disabled = true;
                    }
                    return hor;
                })
            }
        }
        
        return horarios;
    }

    public validarHorarioConDuracion(listaHorario: Hora[], idHora: number, duracion: number): boolean {
        // Verificar si entre los dos id hay un disabled
        const hora = listaHorario.find(item => item.id === idHora);
        const horaInicio = moment(hora.value,'HH:mm');
        const horaFinFormat = horaInicio.add(duracion, 'minutes').format('HH:mm');
        const horaFin = moment(horaFinFormat,'HH:mm');
        // Buscamos el id más cercado que no sea disabled
        const horarioCercado = this.obtenerHorarioRango(hora.value,horaFin.format('HH:mm'),listaHorario);
        const horaCercana = horarioCercado; // Obtenemos el reigstro más cercano

        console.log('VALIDAR',horarioCercado);
        return horaCercana.length > 0 ? true : false;
    }

    private obtenerHorarioRango(horaInicio: string, horaFin: string, listaHorario: Hora[]): Hora[] {
        let listaResult: Hora[] = [];
        let horaInicioMoment = moment(horaInicio,'HH:mm');
        let horaFinMoment = moment(horaFin,'HH:mm');
        for (const hora of listaHorario) {
            let horaValue = moment(hora.value,'HH:mm');
            if( (horaInicioMoment < horaValue && horaValue <= horaFinMoment) && hora.disabled === true ) {
                listaResult.push(hora);
            }
        }
        return listaResult;
    }

    private  calcularStringHora(hora: number): string {
        if(hora === 0) {
            return "00"
        }
        return hora.toString();
    }

    
}