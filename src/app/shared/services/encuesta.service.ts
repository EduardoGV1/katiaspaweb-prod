import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseService } from './base.service';
import { ENCUESTA_URL } from 'src/app/utils/url_constants';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService extends BaseService {

  constructor(
    private http   : HttpClient
  ) {
    super();
  }

  registrarEncuesta(encuesta): Promise<any> {
    return this.http.post(`${ENCUESTA_URL}GenerarNuevaEncuesta`, encuesta, {headers: this.obtenerHeaders()} ).toPromise();
  }

  getEncuestaPaginado({
    pages = 1,
    rows = 10
  }): Promise<any> {
    let params = new HttpParams();
        params = params.append('Pages',pages.toString());
        params = params.append('Rows',rows.toString());
    return this.http.get(`${ENCUESTA_URL}GetEncuestaPaginado`, { params, headers: this.obtenerHeaders()} ).toPromise();
  }

  getEncuestaById(encuestaId: number): Promise<any> {
    let params = new HttpParams();
        params = params.append('EncuestaId',encuestaId.toString());
    return this.http.get(`${ENCUESTA_URL}ObtenerEncuestaById`, { params, headers: this.obtenerHeaders()} ).toPromise();
  }

  getEncuestaResultById(encuestaId: number): Promise<any> {
    let params = new HttpParams();
        params = params.append('EncuestaId',encuestaId.toString());
    return this.http.get(`${ENCUESTA_URL}ObtenerResultadoEncuestaById`, { params, headers: this.obtenerHeaders()} ).toPromise();
  }

}
