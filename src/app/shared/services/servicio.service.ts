import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { CATEGORIASERVICIOS_URL, CITA_URL, CLIENTE_URL, SERVICIO_URL } from 'src/app/utils/url_constants';
import { INewServicio } from '../models/response';

@Injectable({
  providedIn: 'root'
})
export class ServicioService extends BaseService {

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  listarServiciosPaginado(
    obj: {
      isActivo: number;
      categoriaServicioID: number;
      genero: string;
      pagina: number;
      cantidad: number;
    }): Promise<any> {
      debugger
    return this.http.post(`${SERVICIO_URL}ListarServiciosPaginado`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  listarServiciosActivos(obj: { genero: boolean }): Promise<any> {
    return this.http.post(`${SERVICIO_URL}ListarServiciosActivos`, { headers: this.obtenerHeaders()}).toPromise();
  }

  listarCategoriaServiciosDropdown(): Promise<any> {
    return this.http.post(`${SERVICIO_URL}ListarCategoriaServicioDroppdown`, { headers: this.obtenerHeaders() }).toPromise();
  }

  registrarServicio(
      obj: INewServicio
    ): Promise<any> {
    return this.http.post(`${SERVICIO_URL}RegistrarServicio`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  editarServicio(
      obj: INewServicio
    ): Promise<any> {
    return this.http.post(`${SERVICIO_URL}ActualizarServicio`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }


  obtenerServicioPorIDWeb(servicioId): Promise<any> {
    const obj = {
      servicioID: servicioId
    }
    return this.http.post(`${SERVICIO_URL}ObtenerServicioPorIDWeb`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  deleteServicio(servicioId: number): Promise<any> {
    const obj = {
      servicioID: servicioId,
    }
    return this.http.post(`${SERVICIO_URL}EliminarServicio`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  listarCategoriaServicios(
    obj: {
            pagina  : number,
            cantidad: number
            })      : Promise<any> {
    return this.http.post(`${CATEGORIASERVICIOS_URL}ListarCategoriaServicios`, obj, {headers: this.obtenerHeaders()} ).toPromise();
  }

  insertCategoriaServicio(descripcion: string): Promise<any> {
    const obj = {
      accion: "string",
      descripcion: descripcion,
      totalServicios: 0,
      categoriaServicioID: 0
    }
    return this.http.post(`${CATEGORIASERVICIOS_URL}InsertarCategoriaServicio`, obj, {headers: this.obtenerHeaders()} ).toPromise();
  }

  updateCategoriaServicio(idCatServicio:number, descripcion: string): Promise<any> {
    const obj = {
      accion: "string",
      descripcion: descripcion,
      totalServicios: 0,
      categoriaServicioID: idCatServicio
    }
    return this.http.post(`${CATEGORIASERVICIOS_URL}ActualizarCategoriaServicio`, obj, {headers: this.obtenerHeaders()} ).toPromise();
  }

  deleteCategoriaServicio(categoriaId: number): Promise<any> {
    const obj = {
      categoriaServicioID: categoriaId,
    }
    return this.http.post(`${CATEGORIASERVICIOS_URL}EliminarCategoriaServicio`, obj, {headers: this.obtenerHeaders()} ).toPromise();
  }


  listarClientesPaginado(obj: {
    genero: boolean,
    apellidos: string,
    nombres: string,
    fechaInicio: string,
    fechaFin: string,
    start: number,
    length: number
  }): Promise<any> {
    return this.http.post(`${CLIENTE_URL}ListarClientePaginado`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  obtenerClientexID(obj: {
    clienteID: number
  }): Promise<any> {
    return this.http.post(`${CLIENTE_URL}PaginadoObtenerClientePorID`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  obtenerCitasxClienteID(obj: {
    clienteID: number
  }): Promise<any> {
    return this.http.post(`${CITA_URL}ObtenerCitasGeneralesWebPorClienteID`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  listarReporteCalificacionPaginado(obj:{
    pagina:number,
    cantidad:number
  }):Promise<any>{
    return this.http.post(`${SERVICIO_URL}ListarReporteCalificacionPaginado`,obj,{headers:this.obtenerHeaders()}).toPromise();
  }
}