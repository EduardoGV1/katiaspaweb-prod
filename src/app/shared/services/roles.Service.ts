import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseService } from './base.service';
import { USUARIO_URL } from 'src/app/utils/url_constants';
@Injectable({
    providedIn: 'root'
  })

  export class RolesService extends BaseService{
    constructor(
        private http: HttpClient
      ) {
        super();
      }

      listarRoles(data:{
          Start:Number,
          Length:Number
      }):Promise<any> {
          let params = new HttpParams()
          params=params.append("Start",data.Start.toString());
          params=params.append("Length",data.Length.toString());
          return this.http.get(`${USUARIO_URL}ListarRoles`, {params,headers: this.obtenerHeaders()} ).toPromise();
      }

      agregarRol(data:{
        descripcion:string
      }):Promise<any>{
        return this.http.post(`${USUARIO_URL}RegistrarRol`, data,{headers: this.obtenerHeaders()} ).toPromise();
      }
      eliminarRol(data:{
        rolID:Number
      }):Promise<any>{
        return this.http.post(`${USUARIO_URL}EliminarRol`, data,{headers: this.obtenerHeaders()} ).toPromise();
      }
      editarRol(data:{
        rolID:Number
        descripcion:string
      }):Promise<any>{
        return this.http.post(`${USUARIO_URL}ActualizarRol`, data,{headers: this.obtenerHeaders()} ).toPromise();
      }
  }