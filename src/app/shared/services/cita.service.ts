import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseService } from './base.service';
import { CITA_URL } from 'src/app/utils/url_constants';

@Injectable({
  providedIn: 'root'
})
export class CitaService extends BaseService {

  constructor(private http: HttpClient) { super(); }

  obtenerCitasWebPorFecha(obj: { fechaInicio: string, fechaFin: string } ): Promise<any> {
    return this.http.post(`${CITA_URL}ObtenerCitasWebPorRango`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  obtenerPersonalPorServicio(opc: number): Promise<any> {
    return this.http.post(`${CITA_URL}ObtenerPersonalPorServicio`, opc, {headers: this.obtenerHeaders()}).toPromise();
  }

  obtenerHorarioOcupadoReservaCitaWeb(opc: {  
    fecha: string,
    servicioID: number;
    trabajadorId: number;
    clienteId: number;
  }): Promise<any> {
    return this.http.post(`${CITA_URL}ObtenerHorarioOcupadoReservaCitaWeb`, opc, {headers: this.obtenerHeaders()}).toPromise();
  }

  generarCitaWeb(opc: {
    "servicioID": number,
    "clienteID": number,
    "usuarioID": number,
    "fechaHora": Date,
    "tipoPago": number,
    "pagoCuota": number,
    "consultorioID": number
  }): Promise<any> {
    let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiIyIiwibmJmIjoxNjI1MDE3NzA4LCJleHAiOjE2MjU0NDk3MDgsImlhdCI6MTYyNTAxNzcwOH0.A5paTJeAiqg6e5F9INZrzQmodSd9n2uvsioma1JxVvA";
    let headers = new HttpHeaders()
                      .set('Content-Type', 'application/json')
                      .set('Accept', 'application/json')
                      .set('Authorization','Bearer' + token)
    return this.http.post(`${CITA_URL}GenerarCitaWeb`, opc, {headers}).toPromise();
  }

  reporgramarCitaWeb(opc: {
    "citaSesionID": number,
    "clienteID": number,
    "servicioID": number,
    "usuarioID": number,
    "oldUsuarioID": number,
    "consultorioID": number
    "fechaHora": Date,
  }): Promise<any> {
    console.log('OBJ',opc);
    let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiIyIiwibmJmIjoxNjI1MDE3NzA4LCJleHAiOjE2MjU0NDk3MDgsImlhdCI6MTYyNTAxNzcwOH0.A5paTJeAiqg6e5F9INZrzQmodSd9n2uvsioma1JxVvA";
    let headers = new HttpHeaders()
                      .set('Content-Type', 'application/json')
                      .set('Accept', 'application/json')
                      .set('Authorization','Bearer' + token)
                      return this.http.post(`${CITA_URL}ReprogramarSesionWeb`, opc, {headers}).toPromise();
  }

  validarDevolucionCita(opc: {
    "citaID": number
  }): Promise<any> {
    let headers = new HttpHeaders()
                      .set('Content-Type', 'application/json')
                      .set('Accept', 'application/json');
    return this.http.post(`${CITA_URL}ValidarDevolucionCita`, opc, {headers}).toPromise();
  }

  cancelarCita(opc: {
    "citaID": number,
    "puedeDevolverMitad": boolean
  }): Promise<any> {
    let headers = new HttpHeaders()
                      .set('Content-Type', 'application/json')
                      .set('Accept', 'application/json');
    return this.http.post(`${CITA_URL}CancelarCita`, opc, {headers}).toPromise();
  }

  obtenerCitasWebPaginado(obj:{pagina:number,registros:number,estado?:string,fechaInicio?:string,fechaFin?:string,clienteNombres?:string,servicioID?:string,estadoPago?:string,numCita?:string,numConsultorio?:string,usuarioID?:string}): Promise<any> {
    return this.http.post(`${CITA_URL}ObtenerCitasWebPaginado`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  obtenerSesionesxCitaWeb(obj:{citaID:number}): Promise<any> {
    return this.http.post(`${CITA_URL}ObtenerSesionesxCita`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  habilitarNuevaSesion(obj:{citaID:number,fechaInicio:Date,tomarMismoNroSesion:boolean,fechaFin?:Date}): Promise<any> {
    return this.http.post(`${CITA_URL}HabilitarNuevaSesion`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  registrarPago(obj:{TipoPago:number,PagoServicioID:number,fechaPago:Date}): Promise<any>{
    return this.http.post(`${CITA_URL}RegistrarPago`, obj, { headers: this.obtenerHeaders() }).toPromise();
  }

  registrarDevolucion(obj:{pagoServicioID:number,monto:number,comentario:string}):Promise<any>{
    return this.http.post(`${CITA_URL}RegistrarDevolucion`,obj,{ headers: this.obtenerHeaders() }).toPromise();
  }

  obtenerEstadosCitaSesion():Promise<any>{
    return this.http.post(`${CITA_URL}ObtenerEstadosCitaSesion`,{ headers: this.obtenerHeaders() }).toPromise();
  }

  obtenerEstadosCitas():Promise<any>{
    return this.http.post(`${CITA_URL}ObtenerEstadosCita`,{ headers: this.obtenerHeaders() }).toPromise();
  }

  cambiarEstadoCitaSesion(data):Promise<any>{
    return this.http.post(`${CITA_URL}ActualizarEstadoSesion`,data,{ headers: this.obtenerHeaders() }).toPromise();
  }

  asignarNuevaSesion(data):Promise<any>{
    return this.http.post(`${CITA_URL}AsignarNuevaSesion`,data,{ headers: this.obtenerHeaders() }).toPromise();
  }

}
