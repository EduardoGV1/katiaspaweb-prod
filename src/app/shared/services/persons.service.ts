import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseService } from './base.service';
import { USUARIO_URL } from 'src/app/utils/url_constants';

@Injectable({
  providedIn: 'root'
})
export class PersonsService extends BaseService {

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  listarTrabajadorPaginado(data: {
    rolID: number;
    fechaInicio: Date;
    fechaFin: Date;
    genero: number;
    start: number;
    length: number;
    isAdmin: boolean;
  }): Promise<any> {
    return this.http.post(`${USUARIO_URL}ListarTrabajadorPaginado`, data, { headers: this.obtenerHeaders() }).toPromise();
  }

  obtenerTrabajadorPorID(data: {
    usuarioID: number;
  }): Promise<any> {
    return this.http.post(`${USUARIO_URL}PaginadoObtenerTrabajadorPorID`, data, { headers: this.obtenerHeaders() }).toPromise();
  }

  actualizarPerfilTrabajador(data: {
    workerID: number;
    photo:string;
    name:string;
    surname:string;
    email:string;
    phone:number;
    fechaNac:string;
    direccion:string;
    genero:boolean;
    existeFoto:boolean;
    rolID:string;

  }): Promise<any> {
    return this.http.post(`${USUARIO_URL}ActualizarPerfilTrabajador`, data, { headers: this.obtenerHeaders() }).toPromise();
  }

  listarTrabajadorPaginadoByNombres(data: {
    nombre: String;
    apellido: String;
    genero: number;
    start: number;
    length: number;
    isAdmin: boolean;
  }): Promise<any> {
    return this.http.post(`${USUARIO_URL}ListarTrabajadorPaginadoByNombres`, data, { headers: this.obtenerHeaders() }).toPromise();//FALTA IMPLEMENTAR SERVICIO
  }

  listarServiciosDisponiblesTrabajador(data: {
    usuarioID: number
  }): Promise<any> {
    return this.http.post(`${USUARIO_URL}ListarServiciosDisponiblesTrabajador`, data, { headers: this.obtenerHeaders() }).toPromise();
  }

  listarServiciosAsignadosTrabajador(data: {
    usuarioID: number
  }): Promise<any> {
    return this.http.post(`${USUARIO_URL}ListarServiciosAsignadosTrabajador`, data, { headers: this.obtenerHeaders() }).toPromise();
  }

  asignarServicio(data: {
    servicioID: number;
    usuarioID: number;
  }): Promise<any> {
    return this.http.post(`${USUARIO_URL}AsignarServicio`, data, { headers: this.obtenerHeaders() }).toPromise();
  }

  quitarServicio(data: {
    servicioID: number;
    usuarioID: number;
  }): Promise<any> {
    return this.http.post(`${USUARIO_URL}QuitarServicio`, data, { headers: this.obtenerHeaders() }).toPromise();
  }

  agregarTrabajador(data: {
    nombres: string;
    apellidos: string;
    dni: number;
    telefono: number;
    correo: string;
    fechaRegistroString: string;
    genero: boolean;
    rol: {
      rolID: number
    };
    foto: string;
    fechaNacimiento: Date,
    direccion: string

  }): Promise<any> {
    return this.http.post(`${USUARIO_URL}RegistrarTrabajador`, data, { headers: this.obtenerHeaders() }).toPromise();
  }

  enviarPasswordTrabajador(data: {
    usuarioID: number
  }): Promise<any> {
    return this.http.post(`${USUARIO_URL}EnviarCredencialesUsuarioWeb`, data, { headers: this.obtenerHeaders() }).toPromise();
  }
  cambiarPasswordTrabajador(data: {
    usuarioID: number,
    password: string
  }): Promise<any> {
    return this.http.post(`${USUARIO_URL}UpdatePasswordTrabajador`, data, { headers: this.obtenerHeaders() }).toPromise();
  }

  obtenerHorarioPorTrabajadorAll():Promise<any>{
    return this.http.get(`${USUARIO_URL}ObtenerHorariosTrabajadores`).toPromise();
  }

  obtenerHorarioPorTrabajadorById(data:{
    usuarioID: number
  }):Promise<any>{
    const params = new HttpParams()
    .set('id',data.usuarioID.toString())
    return this.http.get(`${USUARIO_URL}obtenerHorarioPorTrabajador`,{params}).toPromise();
  }

  registrarHorarioTrabajador(data):Promise<any>{
    return this.http.post(`${USUARIO_URL}RegistrarHorarioTrabajador`, data, { headers: this.obtenerHeaders() }).toPromise();
  }

  obtenerEspecialistas():Promise<any>{
    return this.http.post(`${USUARIO_URL}ObtenerEspecialistas`, { headers: this.obtenerHeaders() }).toPromise();
  }

}