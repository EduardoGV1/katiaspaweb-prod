import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { DASHBOARD_URL } from 'src/app/utils/url_constants';


@Injectable({
    providedIn: 'root'
  })

  export class DashboardService extends BaseService{
    constructor(
        private http: HttpClient
      ) {
        super();
      }

      obtenerDataDashboard(): Promise<any>{
        return this.http.get(`${DASHBOARD_URL}ObtenerDashboard`,  {headers: this.obtenerHeaders()} ).toPromise();
      }
  }