import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { USUARIO_URL } from 'src/app/utils/url_constants';
import { removeModulos, removeUserData } from '../../utils/storage';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {

  constructor(
    private http   : HttpClient,
    private _router: Router
  ) {
    super();
  }

  authUser(login): any {
    return this.http.post(`${USUARIO_URL}IngresarUsuario`, login, {headers: this.obtenerHeaders()} ).toPromise();
  }

  logOut() {
    removeModulos();
    removeUserData();
    this._router.navigate(['/Auth']);
  }
}
