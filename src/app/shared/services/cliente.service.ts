import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CLIENTE_URL } from "src/app/utils/url_constants";
import { BaseService } from "./base.service";

@Injectable({
    providedIn: 'root'
})
export class ClienteService extends BaseService {

    constructor(private http: HttpClient) { super(); }

    listarClientesActivos(): Promise<any> {
        return this.http.post(`${CLIENTE_URL}ListarClientesActivos`, { headers: this.obtenerHeaders() }).toPromise();
    }
    agregarCliente(data:{
        nombres:string;
        nombreFull:string;
        apellidos:string;
        correo:string;
        numeroDocumento:string;
        fechaNac?:Date;
        fechaNacString:string;
        genero:boolean;
        direccion:string;
        foto?:string;
        telefono:number;
        password:string;
        fechaRegistro:Date;
        fechaRegistroString?:String;
    }): Promise<any>{
        return this.http.post(`${CLIENTE_URL}RegistrarClienteWeb`,data,{headers:this.obtenerHeaders()}).toPromise();
    }

}