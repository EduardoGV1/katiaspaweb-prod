import { Component, Input } from '@angular/core';
import { IModules } from '../models/response';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

  @Input() modulos: Array<IModules> = []
  constructor(
    private _router: Router
  ) { }

}
