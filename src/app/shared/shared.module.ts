import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { AntdModule } from './antd.module'
import { MaterialModule } from './material.module';
import { allIcons } from 'angular-feather/icons';
import { FeatherModule } from 'angular-feather';
import { ModalBodyComponent } from './components/modal/modal-body.component';
import { DropZoneDirective } from './directives/drop-zone.directive';
import { FileInputComponent } from './components/file-input/file-input.component';
import { OnlyNumbersDirective } from './directives/only-numbers.directive';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../../environments/environment';
import { MensajeValidacionComponent } from './components/mensajeValidacion/mensajeValidacion.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { StarsComponent } from './components/starts/stars.component';

const MODULES = [AntdModule, MaterialModule, IvyCarouselModule,NgxChartsModule]
const COMPONENTS = [FileInputComponent, DropZoneDirective, ModalBodyComponent, OnlyNumbersDirective, MensajeValidacionComponent,StarsComponent]

@NgModule({
  declarations: [COMPONENTS],
  imports: [...MODULES, 
            FeatherModule.pick(allIcons),
            CommonModule,
            AngularFireModule.initializeApp(environment.firebase)],
  exports: [...MODULES,
            COMPONENTS,
            FeatherModule,
          ]
})
export class SharedModule {}
