import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[onlyNumbers]'
})
export class OnlyNumbersDirective {

    @Input('max') max;

    @HostListener('keydown', ['$event'])
    onKeyDown(event) {
        const evt = <KeyboardEvent>event;
        const numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
        const alts = ["Backspace", "Tab", "Enter", "ArrowLeft", "ArrowRight","."];
        const target = evt.target as HTMLInputElement;

        if(numbers.includes(evt.key)  || alts.includes(evt.key)) {
            evt.stopPropagation();
    
            if (evt.key === "." && target.value.includes(".")) {
                return evt.preventDefault();
            }
    
            if (target.value.length > 10 && alts.indexOf(evt.key) === -1) {
                return evt.preventDefault();
            }
            
            if(this.max && (Number(target.value + evt.key) > this.max)) {
                return evt.preventDefault();
            }
        } else {
            return evt.preventDefault();
        }
    }
}