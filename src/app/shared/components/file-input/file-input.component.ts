import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'file-input',
    templateUrl: './file-input.component.html'
})
export class FileInputComponent {

    fotosArr: FileList
    arrFiles = []
    @Output() emitFotos: EventEmitter<any> = new EventEmitter<any>()

    constructor() { }

    onFileSelected() {
        const inputNode: HTMLInputElement = document.querySelector('#fotos_list');
        this.fotosArr = inputNode.files;
        this.fileToB64()
    }

    addFotos(file: File[]) {
        const inputNode: HTMLInputElement = document.querySelector('#fotos_list');
        let list = new DataTransfer();

        file.forEach(item => list.items.add(item))
        
        let myFileList  = list.files
        inputNode.files = myFileList

        this.fotosArr   = inputNode.files
        this.fileToB64()
    }

    fileToB64() {
        this.arrFiles = []
        for (let index = 0; index < this.fotosArr.length; index++) {
            const element = this.fotosArr[index];
            let reader  = new FileReader()
            reader.readAsDataURL(element)
            reader.onloadend = () => { 
                const newFile = reader.result;
                this.arrFiles.push({
                    path: newFile
                })
                if(index === (this.fotosArr.length - 1)) {
                    setTimeout(() => {
                        this.emitFotos.emit(this.arrFiles)
                    }, 300);
                }
            }
        }
    }

}