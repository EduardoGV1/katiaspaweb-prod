import { Component, Input } from '@angular/core';

@Component({
  selector: 'stars',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.scss']
})
export class StarsComponent {
    @Input() rating: number = 0;
    fullStars: number = 0;
    halfStar: boolean = false;
  
    ngOnInit() {
      this.calculateStars();
    }
  
    calculateStars() {
      this.fullStars = Math.floor(this.rating);
      const remainder = this.rating - this.fullStars;
  
      if (remainder >= 0.5) {
        this.halfStar = true;
      }
    }

    arrayLength(n: number): any[] {
        return new Array(n);
      }
}