import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesModule } from '../pages.module';
import { SharedModule } from '../../shared/shared.module';
import { VentasComponent } from './ventas/ventas.component';
import { VentasRoutingModule } from './ventas.routing';

// Component


@NgModule({
  declarations: [
    VentasComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PagesModule,
    VentasRoutingModule   
  ],

})
export class VentasModule { }
