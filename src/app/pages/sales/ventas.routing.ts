import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from '../pages.component';
import { VentasComponent } from './ventas/ventas.component';

const routes: Routes = [
    {
        path     : '',
        component: PagesComponent,
        children : [
            { 
                path        : '',
                component   : VentasComponent, 
                data        : {breadcrumb: "Historial de Ventas"}               
                
            }
        ],
        data: {breadcrumb: "Ventas"}
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class VentasRoutingModule { }