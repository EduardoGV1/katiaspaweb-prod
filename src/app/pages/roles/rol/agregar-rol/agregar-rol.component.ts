import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { IApiResponse } from "src/app/shared/models/response";
import { RolesService } from "src/app/shared/services/roles.Service";
import { __messageSnackBar } from "src/app/utils/helpers";

@Component({
    selector: 'app-agregar-rol',
    templateUrl: './agregar-rol.component.html'
})

export class AgregarRolComponent implements OnInit{
    changeData = false
    formularioGroup:FormGroup

    constructor(
        private _rolesService: RolesService,
        public dialogRef: MatDialogRef<AgregarRolComponent>,
        private _matSnackBar: MatSnackBar,
    ){
        this.formularioGroup=new FormGroup({
            descripcion : new FormControl(null,[Validators.required])
        })

    }
    async ngOnInit(){

    }

    async registrarRol(){
        try {
            const{
                Identificador,
                Mensaje,
                Descripcion
            }:IApiResponse=await this._rolesService.agregarRol({
                descripcion:this.formularioGroup.get('descripcion').value
            })
            if (Identificador==="success") {
                this.dialogRef.close()
                this.changeData=true
            }
            return __messageSnackBar(this._matSnackBar, Descripcion)
        } catch (error) {
            console.log(error)
        } finally{
            this.formularioGroup.enable()
        }

    }
}