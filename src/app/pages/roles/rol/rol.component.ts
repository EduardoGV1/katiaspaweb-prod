import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IApiResponse, IRoles } from 'src/app/shared/models/response';
import { RolesService } from 'src/app/shared/services/roles.Service';
import { __messageSnackBar } from 'src/app/utils/helpers';
import { AgregarRolComponent } from './agregar-rol/agregar-rol.component';
import { EditarRolComponent } from './editar-rol/editar-rol.component';

export interface DialogData1{
    rolID:number,
    descripcion:string
}
@Component({
    selector:'app-rol',
    templateUrl:'rol.component.html'
})

export class RolComponent implements OnInit{
    rolesArr:any=[]
    apellidos= new FormControl(null)
    rolID=0
    descripcion=""

    constructor(
        private _rolesService: RolesService,
        public  dialog         : MatDialog,
        private _matSnackBar: MatSnackBar,
    ){}
    
    async ngOnInit(){
        try {
            const{
                Identificador,
                Mensaje,
                Descripcion,
                Roles
            }:IRoles=await this._rolesService.listarRoles({
                Start:1,
                Length:10
            })
            if(Identificador=="success"){
                console.log(Roles)
                this.rolesArr=Roles
            }
        } catch (error) {
            console.log(error)
        }

    }
    agregarRol(){
        const dialogRef = this.dialog.open(AgregarRolComponent,{disableClose:true})
        dialogRef.afterClosed().subscribe(() => {
          if(dialogRef.componentInstance.changeData) this.ngOnInit()
        })
    }
    
    editarRol(rolID:number,descripcion:string){
        const dialogRef = this.dialog.open(EditarRolComponent,{
            disableClose:true,
        data:{rolID:rolID,descripcion:descripcion}})
        dialogRef.afterClosed().subscribe(() => {
          if(dialogRef.componentInstance.changeData) this.ngOnInit()
        })
    }

    async desactivarRol(rolID:number){
        try {
            const{
                Identificador,
                Mensaje,
                Descripcion
            }:IApiResponse=await this._rolesService.eliminarRol({
                rolID:rolID
            })
            if (Identificador==="success") {
                this.ngOnInit()
            }
            return __messageSnackBar(this._matSnackBar, Descripcion)
        } catch (error) {
            console.log(error)
        }
        console.log(rolID)
    }
    
}