import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Component
import { SharedModule } from '../../shared/shared.module';
import { PagesModule } from '../pages.module';
import { RolesRoutingModule } from './roles.routing';
import { RolComponent } from './rol/rol.component';
import { AgregarRolComponent } from './rol/agregar-rol/agregar-rol.component';
import { EditarRolComponent } from './rol/editar-rol/editar-rol.component';

@NgModule({
  declarations: [
    RolComponent,
    AgregarRolComponent,
    EditarRolComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PagesModule,
    RolesRoutingModule
   
  ],

})
export class RolModule { }