export interface IClienteCita {
    ClienteCulqiID:      null;
    ClienteID:           number;
    Nombres:             string;
    NombreFull:          null;
    Apellidos:           string;
    Correo:              null;
    FechaNac:            Date;
    FechaNacString:      null;
    Genero:              boolean;
    Direccion:           null;
    GeneroString:        null;
    Foto:                null | string;
    Telefono:            number;
    Password:            null;
    Tipo:                number;
    Token:               null;
    FechaRegistro:       Date;
    FechaRegistroString: null;
    Accion:              null;
  }
  
  export interface IServicioCita {
    ServicioID:        number;
    Nombre:            string;
    Descripcion:       null;
    Foto:              null;
    UrlFotoss:         null;
    IsActivo:          boolean;
    CategoriaServicio: null;
    DetalleServicio:   IDetalleServicio;
    Accion:            null;
    PuedeEliminar:     boolean;
    Especialistas:     null;
  }
  
  export interface IDetalleServicio {
    DetalleServicioID: number;
    Precio:            number;
    Duracion:          number;
    Genero:            null;
    GeneroString:      null;
    ServicioID:        number;
    TiempoDescanso:    number;
    NroSesionesAprox:  number;
    IsFavourite:       boolean;
    PagoCuota:         boolean;
  }

  export interface IPersonalCita {
    UsuarioID:           number;
    DNI:                 number;
    Telefono:            number;
    Correo:              null;
    Password:            null;
    Foto:                string;
    FechaRegistro:       Date;
    FechaRegistroString: null;
    Rol:                 IRol;
    Username:            null;
    Genero:              boolean;
    GeneroString:        null;
    IsActivo:            boolean;
    Nombres:             string;
    NombreFull:          null;
    Apellidos:           string;
    Token:               null;
    Accion:              null;
}

export interface IRol {
    RolID:       number;
    Descripcion: string;
}

export interface IPersonal {
    ConsultorioID:  number;
    NroConsultorio: number;
    HorarioOcupado: IHorarioOcupado[];
}

export interface IHorarioOcupado {
    FechaHora:       Date;
    Duracion:        number;
    ReservaSesionID: number;
}