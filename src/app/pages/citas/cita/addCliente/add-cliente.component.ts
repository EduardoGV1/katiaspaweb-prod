import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import * as moment from "moment";
import { IApiResponse } from "src/app/shared/models/response";
import { ClienteService } from "src/app/shared/services/cliente.service";
import { __messageSnackBar } from "src/app/utils/helpers";
const ToDay=moment(new Date()).add(-18,'years').toDate()

@Component({
    selector:'app-add-cliente',
    templateUrl:'./add-cliente.component.html'
})

export class AddClienteComponent implements OnInit{

    clientesAddForm: FormGroup;
    disabledEndDate = (startValue: Date): boolean => {
        if (!startValue || !ToDay) {
          return false;
        }
        return startValue.getTime() > ToDay.getTime();
      };

    constructor(
        private _clienteService:ClienteService,
        public datePipe: DatePipe,
        public dialogRef: MatDialogRef<AddClienteComponent>,
        private _matSnackBar: MatSnackBar,
    ){
        this.clientesAddForm = new FormGroup({
            nombre: new FormControl(null,[Validators.required,Validators.min(1000)]),
            fecha:new FormControl(),
            apellido: new FormControl(null, [Validators.required]),
            dni: new FormControl(null, [Validators.required]),
            telefono: new FormControl(null, [Validators.required]),
            correo: new FormControl(null, [Validators.required]),
            genero: new FormControl(null, [Validators.required]),
            direccion: new FormControl(null, [Validators.required]),
            respuestaEncuesta: new FormControl(),
        })
    }
    ngOnInit(): void {
        console.log(this.clientesAddForm.get('nombre'))
    }
    close(){}
    async registrarCliente(){
        const _genero = (this.clientesAddForm.value.genero == 'true')?true:false
        console.log(this.clientesAddForm)
        console.log(this.clientesAddForm.value)
        this.clientesAddForm.disable();
        try {
            const {
                Mensaje,
                Identificador,
                Descripcion
            } : IApiResponse = await this._clienteService.agregarCliente({
                nombres: this.clientesAddForm.value.nombre,
                apellidos:this.clientesAddForm.value.apellido,
                numeroDocumento:this.clientesAddForm.value.dni,
                telefono:Number(this.clientesAddForm.value.telefono),
                correo:this.clientesAddForm.value.correo,
                direccion:this.clientesAddForm.value.direccion,
                fechaNacString:this.datePipe.transform(this.clientesAddForm.value.fecha, 'yyyy-MM-dd'),
                genero:_genero,
                nombreFull:this.clientesAddForm.value.nombre+" "+this.clientesAddForm.value.apellido,
                password:"admin123",
                fechaRegistro:new Date(),
                fechaRegistroString:this.datePipe.transform(new Date(), 'yyyy-MM-dd hh:mm')
            })

            if (Identificador==="success") {
                this.dialogRef.close();
                // this.changeData=true;
                this.clientesAddForm.enable();
            }
            return __messageSnackBar(this._matSnackBar,Descripcion)
        } catch (error) {
            // return __messageSnackBar(this._matSnackBar,"Error al guardar cliente")
        } finally {
            this.clientesAddForm.enable();
        }
    }


}