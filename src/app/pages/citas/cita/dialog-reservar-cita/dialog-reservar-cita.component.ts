import { Component, Inject, OnInit } from '@angular/core';
import { CitaService } from '../../../../shared/services/cita.service';
import { IClienteCita, IPersonalCita, IServicioCita, IPersonal } from '../../interface/interface';
import { ClienteService } from '../../../../shared/services/cliente.service';
import { ServicioService } from '../../../../shared/services/servicio.service';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Hora, Horario } from 'src/app/shared/classes/horario';
import * as moment from 'moment';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Cita } from '../cita.component';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { AddClienteComponent } from '../addCliente/add-cliente.component';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',

  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
}

@Component({
  selector: 'app-dialog-reservar-cita',
  templateUrl: './dialog-reservar-cita.component.html',
  styleUrls: ['./dialog-reservar-cita.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class DialogReservarCitaComponent implements OnInit {
  listaCliente: IClienteCita[];
  listaServicios: IServicioCita[];
  listaPersonal: IPersonalCita[] = [];
  listaHorarioOcupado: IPersonal[] = [];
  formReserva: FormGroup;
  listaHorario: Hora[];
  listaHorarioInicio: Hora[];
  listaHorarioFin: Hora[];
  fechaSeleccionada: string;
  tipo: number;
  citaDetalle: Cita;
  consultorioIdDetalle: number;
  fechaSeleccionadaString: string;
  termino: string = '';
  listaClienteFind: IClienteCita[] = [];
  horario = new Horario();
  constructor(private citaService: CitaService, private clienteService: ClienteService,
    private servicioService: ServicioService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogReservarCitaComponent>,
    private spinner: NgxSpinnerService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.crearFormulario();
  }

  async ngOnInit() {
    console.log('DATA', this.data);
    this.tipo = this.data.tipo;
    var march = moment(this.data.fechaHoy);
    var temp = march.locale('es');
    this.fechaSeleccionada = temp.format('dddd, DD MMM YYYY');
    this.fechaSeleccionadaString = temp.format('yyyy-MM-DD');
    this.spinner.show();
    await this.obtenerCliente();
    await this.obtenerServicios();
    this.spinner.hide();
    // Validamos si es creación o si es detalle
    if (this.tipo == 2) {
      this.citaDetalle = this.data.cita;
      this.consultorioIdDetalle = this.data.consultorioId;
      this.formReserva.get('cliente').setValue(this.citaDetalle.ClienteID);
      this.formReserva.get('servicio').setValue(this.data.cita.ServicioID);
      this.formReserva.get('cliente').disable();
      this.formReserva.get('servicio').disable();
      this.cambioServicio(this.formReserva.get('servicio').value);
    }
  }

  crearFormulario() {
    this.formReserva = this.fb.group({
      cliente: [],
      servicio: [],
      dia: [],
      personal: [],
      horaInicio: [],
      horaFin: [],
      termino: []
    })
  }


  async generarCitaWeb() {
    const servicioID = Number.parseInt(this.formReserva.get('servicio').value);
    const clienteID = Number.parseInt(this.formReserva.get('cliente').value);
    const horaInicioId = this.formReserva.get('horaInicio').value;
    const hora = this.listaHorarioInicio.find(item => item.id === horaInicioId);
    const horaRestada = moment(hora.value, 'HH:mm').add(-5, 'hour').format('HH:mm');
    const usuarioID = this.formReserva.get('personal').value;
    const fechaHora = new Date(`${this.data.fechaHoy} ${horaRestada}`);
    const consultorioID = this.listaHorarioOcupado[0].ConsultorioID;
    const tipoPago = 0;
    const pagoCuota = 0;
    if (this.tipo === 1) { // Crear
      this.spinner.show();
      const resp = await this.citaService.generarCitaWeb({ servicioID, clienteID, usuarioID, fechaHora, consultorioID, tipoPago, pagoCuota });
      this.spinner.hide();
      await Swal.fire({
        title: 'Se agregó correctamente',
        icon: 'success',
        timer: 2500
      })
      this.dialogRef.close(1);
      // Swal
    } else { // Reprogramar

      const fechaReprogramada = moment(this.formReserva.get('dia').value).format('yyyy-MM-DD');
      const fechaReprogramadaComparar = moment(fechaReprogramada, 'yyyy-MM-DD');
      const fechaDosMesesDespues = moment(moment().add(60, 'day').format('yyyy-MM-DD'), 'yyyy-MM-DD');
      const horaReprogramada = this.listaHorarioInicio.find(item => item.id === this.formReserva.get('horaInicio').value).value;
      const horaRestadaReprogramada = moment(horaReprogramada, 'HH:mm').add(-5, 'hour').format('HH:mm');
      const fechaHoraString = `${fechaReprogramada} ${horaRestadaReprogramada}`;
      const fechaHora = new Date(fechaHoraString);
      const oldUsuarioID = this.citaDetalle.UsuarioID;
      const citaSesionID = this.citaDetalle.CitaSesionID;
      if (fechaReprogramadaComparar > fechaDosMesesDespues) {
        await Swal.fire({
          title: 'No puede reprogramar una cita a una fecha máximo 60 días',
          icon: 'warning',
          timer: 2500
        })
        return;
      }
      this.spinner.show();
      const resp = await this.citaService.reporgramarCitaWeb({ citaSesionID, servicioID, clienteID, usuarioID, oldUsuarioID, fechaHora, consultorioID });
      this.spinner.hide();
      await Swal.fire({
        title: 'Se reprogramó correctamente',
        icon: 'success',
        timer: 2500
      })
      this.dialogRef.close(1);
    }

  }

  async cambioServicio(idServicio) {
    await this.obtenerPersonalPorServicio(idServicio);
  }

  async cambioPersonal(personalId) {
    this.spinner.show();
    const servicioId = this.formReserva.get('servicio').value;
    const clienteId = this.formReserva.get('cliente').value;
    const servicio = this.listaServicios.find(item => item.ServicioID === servicioId);
    const duracionServicio = servicio.DetalleServicio.Duracion;
    const now = this.data.fechaHoy;
    const resp = await this.citaService.obtenerHorarioOcupadoReservaCitaWeb({ fecha: now, servicioID: servicioId, clienteId: clienteId, trabajadorId: personalId });
    console.log('RESP', resp);
    this.listaHorarioOcupado = resp.Consultorios;
    if (this.listaHorarioOcupado[0].HorarioOcupado.length === 0) {
      this.listaHorarioInicio = this.horario.generarHorasDia(7, 20);
    } else {
      this.listaHorarioInicio = this.horario.bloquearHorariosOcupados(this.listaHorarioOcupado, duracionServicio, this.fechaSeleccionadaString);

    }
    this.spinner.hide();
  }

  async cancelarCita() {
    let citaId = this.citaDetalle.CitaID;
    this.spinner.show();
    // const { Devolucion, Identificador } = await this.citaService.validarDevolucionCita({ citaID: citaId });
    // this.spinner.hide();
    // if (Identificador == "success") {
      // this.spinner.show();
      const { Identificador, Descripcion } = await this.citaService.cancelarCita({ citaID: citaId, puedeDevolverMitad: false });
      this.spinner.hide();
      if (Identificador == "success") {
        await Swal.fire({
          icon: 'success',
          text: Descripcion,
          timer: 2500
        });
        this.dialogRef.close(1);
      } else {
        return await Swal.fire({
          title: 'Alerta!',
          icon: 'warning',
          text: Descripcion,
          timer: 2500
        });
      // }

    }





    // const resp = await this.citaService.cancelarCita()
  }

  cambioHoraInicio(horaId) {
    const servicioId = this.formReserva.get('servicio').value;
    const servicio = this.listaServicios.find(item => item.ServicioID === servicioId);
    const duracionServicio = servicio.DetalleServicio.Duracion;
    const horaInicio = this.listaHorarioInicio.find(item => item.id === horaId).value;
    const horaInicioMoment = moment(horaInicio, 'HH:mm');
    const horaFin = horaInicioMoment.add(duracionServicio, 'minutes').format('HH:mm');
    this.formReserva.get('horaFin').setValue(horaFin);
  }

  async obtenerCliente() {
    const resp = await this.clienteService.listarClientesActivos();
    console.log('Clientes Activos')
    console.log(resp)
    this.listaCliente = resp.Resultado;
  }

  async obtenerServicios() {
    const resp = await this.servicioService.listarServiciosActivos({ genero: true });
    this.listaServicios = resp.Resultado;
    console.log('RESP', resp);
  }

  async obtenerPersonalPorServicio(opc: number) {
    const resp = await this.citaService.obtenerPersonalPorServicio(opc);
    this.listaPersonal = resp.Personal;
  }

  agregarCliente() {
    const dialogRef = this.dialog.open(AddClienteComponent, {
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.obtenerCliente();
    })
  }

  opcionSeleccionada(event: MatAutocompleteSelectedEvent) {

    console.log("evetn", event)
    // this.pokemonService.getPokemonPorId(event.option.value)
    // .subscribe(pokemon =>
    //   this.pokemonSeleccionado=pokemon)

    //   if(this.pokemonSeleccionado){
    //     this.existPokemon=true;
    //   }

    //console.log(event.option.value)
  }
  optionSelected(event: any) {
    console.log("evvvv", event)
    this.formReserva.get('cliente').setValue(event.ClienteID)
  }

  buscarCoincidencia(termino: string) {
    this.listaClienteFind = [];
    let nroCoincidencias: number = 0;
    let iteraciones: number = 0;
    while (nroCoincidencias < 7 && iteraciones < this.listaCliente.length) {
      if (this.listaCliente[iteraciones].Nombres.includes(this.formReserva.get('termino').value)) {
        this.listaClienteFind.push(this.listaCliente[iteraciones])
        nroCoincidencias++;
      }
      iteraciones++;
    }
    console.log(this.listaClienteFind)
    //console.log(this.pokemones)
  }

}
