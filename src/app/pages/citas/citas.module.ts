import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CitaComponent } from './cita/cita.component';
import { CitasRoutingModule } from './citas.routing';

//Calendario
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin  from '@fullcalendar/interaction'; // a plugin!
import { SharedModule } from '../../shared/shared.module';
import { DialogReservarCitaComponent } from './cita/dialog-reservar-cita/dialog-reservar-cita.component';
import { AddClienteComponent } from './cita/addCliente/add-cliente.component';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin
]);



@NgModule({
  declarations: [CitaComponent, DialogReservarCitaComponent,AddClienteComponent],
  imports: [
    CommonModule,
    CitasRoutingModule,
    FullCalendarModule,
    SharedModule
  ],
  exports: [
    FullCalendarModule
  ]
})
export class CitasModule { }
