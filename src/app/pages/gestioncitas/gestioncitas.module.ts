import { NgModule } from '@angular/core'
import { SesionesGestionComponent } from './sesiones/sesiones.component';
import { CitasGestionComponent } from './citas/citas.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { PagesModule } from '../pages.module';
import { GestionCitasRoutingModule } from './gestioncitas.routing';
import { DetalleCitaComponent } from './citas/detalle-cita/detalle-cita.component';
import { PagoComponent } from './citas/pago/pago.component';
import { DevolucionComponent } from './citas/devolucion/devolucion.component';
@NgModule({
    declarations: [
        SesionesGestionComponent,
        CitasGestionComponent,
        DetalleCitaComponent,
        PagoComponent,
        DevolucionComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        PagesModule,
        GestionCitasRoutingModule
    ],
})
export class GestionCitasModule { }