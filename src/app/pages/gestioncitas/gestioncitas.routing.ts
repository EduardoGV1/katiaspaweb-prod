import { RouterModule, Routes } from "@angular/router";
import { PagesComponent } from "../pages.component";
import { CitasGestionComponent } from "./citas/citas.component";
import { SesionesGestionComponent } from "./sesiones/sesiones.component";
import { NgModule } from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            {
                path: 'Citas',
                children: [
                    {
                        path: "",
                        component: CitasGestionComponent,
                        data:{ breadcrumb: "Citas" },
                    }
                ]
            },
            {
                path: 'Sesiones',
                component: SesionesGestionComponent,
                data: { breadcrumb: "Gestionar Sesiones" }
            }
        ],
        data: { breadcrumb: "Gestion de Citas y Sesiones" }
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GestionCitasRoutingModule { }