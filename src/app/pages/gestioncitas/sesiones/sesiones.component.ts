import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { DialogReservarCitaComponent } from "../../citas/cita/dialog-reservar-cita/dialog-reservar-cita.component";
import * as moment from "moment";
import { Cita } from "../../citas/cita/cita.component";

@Component({
    selector:'app-gestionsesiones',
    templateUrl:'./sesiones.component.html'
})
export class SesionesGestionComponent implements OnInit{
    lista = [
        {
            "CitaID": 1,
            "EstadoCita": "En Proceso",
            "FechaRegistro": "01/02/2023",
            "FechaVencimiento": "01/04/2023",
            "SesionActual": 2,
            "CantidadSesiones": 4,
            "EstadoPago": "No Pagado",
            "Monto": 120.00,
            "TipoPago": "Efectivo",
            "IsActivo": 0,
            "ServicioNombre": "Skin Care Total",
            "ClienteNombreApellido": "Greissy Troya Bustamante",
            "ComprobanteID":123
        },
        {
            "CitaID": 2,
            "EstadoCita": "Stand By",
            "FechaRegistro": "01/04/2023",
            "FechaVencimiento": "01/05/2023",
            "SesionActual": 3,
            "CantidadSesiones": 5,
            "EstadoPago": "Pagado",
            "Monto": 150.00,
            "TipoPago": "Efectivo",
            "IsActivo": 1,
            "ServicioNombre": "Masajes Reductores",
            "ClienteNombreApellido": "Eduardo Gonzales Velasquez",
            "ComprobanteID":125
        },
    ];
    listaEstados = [
        {
            label:"En Proceso",
            value:"1"
        },
        {
            label:"Stand By",
            value:"2"
        },
        {
            label:"Cancelado",
            value:"3"
        },
        {
            label:"Terminado",
            value:"4"
        },
        {
            label:"Prueba",
            value:"5"
        }
    ]
    constructor(
        public dialog: MatDialog
    ){

    }
    ngOnInit(): void {
    }

    gestionarSesion(){
        const tipoGestion = 1;//1 es Programacion, 2 es Reprogramacion
        let citaValue: Cita;
        let temp  = undefined;
        let fechaHoy = moment(new Date()).format('yyyy-MM-DD');
        const dialogRef = this.dialog.open(DialogReservarCitaComponent ,{
            width: '50%',
            height: '50%',
            data: {
              fechaHoy: fechaHoy,
              tipo: tipoGestion, // Tipo 1: Creación , Tipo 2: Editar,
              cita: citaValue,
              consultorioId: temp
            }
          });
          dialogRef.afterClosed().subscribe(async  result => {
            if(result == 1) {
              //await this.mostrarEventos();
              console.log("Sesion Gestionada")
      
            }
          })
    }

}