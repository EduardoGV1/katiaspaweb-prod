import { Component } from '@angular/core';
import { domain } from 'process';
import {  IDashboard } from 'src/app/shared/models/response';
import { DashboardService } from 'src/app/shared/services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  loading=false
  saleData:any=[]
  saleDataPie:any=[]
  saleDataCard:any=[]
  constructor(
    private _dashboardService:DashboardService

  ){
  }
  async ngOnInit() {
    
    try {
      const{
        Mensaje,
                Identificador,
                Descripcion,
                CitasPorMesTotal,
                CitasPorYearTotal,
                VentasTotal,
                VentasMensualesTotal,
                CitasTotal,
                SesionesTotal,
                ClientesTotal,
                SesionesPendientesTotal,
                ServiciosPorMesTotal,
      }: IDashboard = await this._dashboardService.obtenerDataDashboard()
      //console.log(ServiciosPorMesTotal,)
        this.saleData={
          color:'blue',
          data:ServiciosPorMesTotal,
          colorScheme:{
            domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
          }
        };
        this.saleDataPie={
          colorScheme : {
            domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
          },
        
          data:CitasPorYearTotal
        };
        this.saleDataCard={
          colorScheme : {
            domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
          },
          cardColor: '#232837',
          data: [VentasTotal,VentasMensualesTotal,CitasTotal,SesionesTotal,ClientesTotal,SesionesPendientesTotal]
        }
        console.log(this.saleData)
        this.loading=true;
    } catch (error) {
      console.log(error)
    }
}

  async obtenerDataDashboard(){
    try {
      const{
        Mensaje,
                Identificador,
                Descripcion,
                CitasPorMesTotal,
                CitasPorYearTotal,
                VentasTotal,
                CitasTotal,
                SesionesTotal,
                ClientesTotal,
                SesionesPendientesTotal,
                ServiciosPorMesTotal,
      }: IDashboard = await this._dashboardService.obtenerDataDashboard()
      console.log(CitasPorMesTotal,
        CitasPorYearTotal,
        VentasTotal,
        CitasTotal,
        SesionesTotal,
        ClientesTotal,
        SesionesPendientesTotal,
        ServiciosPorMesTotal,)
    } catch (error) {
      console.log(error)
    }
  }
}