import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from '../pages.component';
import { EncuestaComponent } from './encuesta/encuesta.component';
import { GenerarEncuestaComponent } from './encuesta/generar-encuesta/generar-encuesta.component';
import { VisualizarEncuestaComponent } from './encuesta/visualizar-encuesta/visualizar-encuesta.component';

const routes: Routes = [
    {
        path     : '',
        component: PagesComponent,
        children : [
            { 
                path    : 'Encuesta',
                children: [
                    {
                        path     : "",
                        component: EncuestaComponent,
                        data    : {breadcrumb: "Encuesta"},
                    },
                    {
                        path     : "nueva",
                        component: GenerarEncuestaComponent,
                        data     : {breadcrumb: "Nueva"}
                    },
                    {
                        path     : ":encuestaId/editar",
                        component: GenerarEncuestaComponent,
                        data     : {breadcrumb: "Editar"}
                    },
                    {
                        path     : ":encuestaId/ver",
                        component: VisualizarEncuestaComponent,
                        data     : {breadcrumb: "Resultados"}
                    }
                ]
            }
        ],
        data: {breadcrumb: "Atencion al Cliente"}
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AtencionRoutingModule { }
