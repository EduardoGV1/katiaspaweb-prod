import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Component
import { SharedModule } from '../../shared/shared.module';
import { PagesModule } from '../pages.module';
import { EncuestaComponent } from './encuesta/encuesta.component';
import { GenerarEncuestaComponent } from './encuesta/generar-encuesta/generar-encuesta.component';
import { AtencionRoutingModule } from './atencion.routing';
import { RadioButtonComponent } from './encuesta/generar-encuesta/pregunta-encuesta/radiobutton-encuesta/radiobutton-encuesta.component';
import { CheckBoxComponent } from './encuesta/generar-encuesta/pregunta-encuesta/checkbox-encuesta/checkbox-encuesta.component';
import { PreguntaComponent } from './encuesta/generar-encuesta/pregunta-encuesta/pregunta-encuesta.component';
import { VisualizarEncuestaComponent } from './encuesta/visualizar-encuesta/visualizar-encuesta.component';
import { CabeceraEncuestaComponent } from './encuesta/visualizar-encuesta/cabecera-encuesta/cabecera-encuesta.component';
import { RespuestaEncuestaComponent } from './encuesta/visualizar-encuesta/respuesta-encuesta/respuesta-encuesta.component';

@NgModule({
  declarations: [
    EncuestaComponent,
    GenerarEncuestaComponent,
    RadioButtonComponent,
    CheckBoxComponent,
    PreguntaComponent,
    VisualizarEncuestaComponent,
    CabeceraEncuestaComponent,
    RespuestaEncuestaComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PagesModule,
    AtencionRoutingModule
  ],

})
export class AtencionModule { }
