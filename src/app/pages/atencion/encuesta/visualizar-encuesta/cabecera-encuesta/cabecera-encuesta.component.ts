import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-cabecera-encuesta',
    templateUrl: 'cabecera-encuesta.component.html'
})

export class CabeceraEncuestaComponent implements OnInit {

    @Input() titulo       : string        = ""
    @Input() fechaRango   : Array<string> = []
    @Input() fechaRegistro: string        = ""

    constructor() { 
    }

    ngOnInit() { }
}