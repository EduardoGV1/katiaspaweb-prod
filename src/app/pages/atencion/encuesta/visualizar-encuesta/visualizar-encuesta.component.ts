import { Component, OnInit } from '@angular/core';
import { EncuestaService } from '../../../../shared/services/encuesta.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IApiResponse } from 'src/app/shared/models/response';

@Component({
    selector   : 'app-visualizar-encuesta',
    templateUrl: './visualizar-encuesta.component.html'
})

export class VisualizarEncuestaComponent implements OnInit {

    encuestaId: number = 0;
    encuestaResult
    constructor(
        private _encuestaService: EncuestaService,
        private _activatedRoute : ActivatedRoute,
        private _router         : Router
    ) { 
        _activatedRoute.params.subscribe(params => {
            if(!!params.encuestaId)
                this.encuestaId = Number(params.encuestaId)
        })
    }

    async ngOnInit() { 
        const {
            Resultado
        }: IApiResponse = await this._encuestaService.getEncuestaResultById(this.encuestaId);
        this.encuestaResult = Resultado;
    }
}