import { CdkDragDrop, moveItemInArray, transferArrayItem } from "@angular/cdk/drag-drop";
import { Component, Input } from "@angular/core";
import { FormArray, FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-checkbox-encuesta',
    templateUrl: './checkbox-encuesta.component.html'
})
export class CheckBoxComponent {
    @Input() formCheckbox: FormArray

    addOption() {
        this.formCheckbox.push(new FormGroup({
          respuestaID: new FormControl(0),
          opcion     : new FormControl(null, [Validators.required])
      }))
    }

    deleteOption(index: number) {
        this.formCheckbox.controls.splice(index, 1)
        this.formCheckbox.updateValueAndValidity()
    }

    drop(event: CdkDragDrop<any[]>) {
        if (event.previousContainer === event.container) {
          moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
          transferArrayItem(event.previousContainer.data,
                            event.container.data,
                            event.previousIndex,
                            event.currentIndex);
        }
        this.formCheckbox.updateValueAndValidity()
      }
}