import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-radiobutton-encuesta',
    templateUrl: './radiobutton-encuesta.component.html'
})
export class RadioButtonComponent {

    @Input() formRadio: FormArray

    constructor() { }

    addOption() {
        this.formRadio.push(new FormGroup({
          respuestaID: new FormControl(0),
          opcion     : new FormControl(null, [Validators.required]),
      }))
    }

    deleteOption(index: number) {
        this.formRadio.controls.splice(index, 1)
        this.formRadio.updateValueAndValidity()
    }

    drop(event: CdkDragDrop<any[]>) {
        if (event.previousContainer === event.container) {
          moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
          transferArrayItem(event.previousContainer.data,
                            event.container.data,
                            event.previousIndex,
                            event.currentIndex);
        }
        this.formRadio.updateValueAndValidity()
      }
}