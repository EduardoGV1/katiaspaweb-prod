import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EncuestaService } from 'src/app/shared/services/encuesta.service';
import { _orderRangeDate, __messageSnackBar } from 'src/app/utils/helpers';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment/moment';
@Component({
    selector   : 'app-generar-encuesta',
    templateUrl: './generar-encuesta.component.html'
})
export class GenerarEncuestaComponent implements OnInit {
    
    show        : number = null
    encuestaId  : number = 0
    formEncuesta: FormGroup

    constructor(
        private _route          : Router,
        private _matSnackBar    : MatSnackBar,
        private _activatedRoute : ActivatedRoute,
        private _encuestaService: EncuestaService
    ) {
        _activatedRoute.params.subscribe(params => {
            if(!!params.encuestaId)
                this.encuestaId = Number(params.encuestaId)
        })
    }

    async ngOnInit() {
        this.initForms();

        if(this.encuestaId > 0) {
            const {
                Encuestas,
                Identificador,
                Descripcion
            }: any = await this._encuestaService.getEncuestaById(this.encuestaId);

            this.formEncuesta.patchValue({
                titulo: Encuestas.Titulo,
                fecha : [Encuestas.FechaInicio, Encuestas.FechaFin]
            })

            Encuestas.Preguntas.forEach(pregunta => {
                this.addPregunta(pregunta)
            })
        } else this.addPregunta();
    }

    initForms() {
        this.formEncuesta = new FormGroup({
            titulo   : new FormControl(null, [Validators.required]),
            fecha    : new FormControl(null, [Validators.required]),
            preguntas: new FormArray([])
        })
    }

    addPregunta(pregunta = null) {
        const newPregunta = new FormGroup({
            preguntaId  : new FormControl(0, [Validators.required]),
            pregunta    : new FormControl(null, [Validators.required]),
            tipoPregunta: new FormControl(1, [Validators.required])
        })

        if(pregunta) {
            newPregunta.patchValue({
                preguntaId  : pregunta.PreguntaEncuestaID,
                pregunta    : pregunta.Pregunta,
                tipoPregunta: pregunta.Tipo
            })
            if(pregunta.Tipo == 1) newPregunta.addControl('respuesta', new FormControl(null))
            else {
                const newRespuesta = []
                pregunta.Respuesta.forEach((respuesta) => {
                    newRespuesta.push(new FormGroup({
                        respuestaID: new FormControl(respuesta.RespuestaDefinidaID),
                        opcion     : new FormControl(respuesta.Opcion, [Validators.required]),
                    }))
                })
                newPregunta.addControl('respuesta', new FormArray(newRespuesta))
            }
        } else
            newPregunta.addControl('respuesta', new FormArray([new FormControl(null)]))

        this.preguntasForm.push(newPregunta)
    }

    deletePregunta(index: number) {
        (this.formEncuesta.get('preguntas') as FormArray)
            .controls.splice(index, 1)
    }

    duplicatePregunta(index: number) {
        const duplicate = this.preguntasForm.controls[index].value;
        const newPregunta: FormGroup = new FormGroup({
            pregunta    : new FormControl(duplicate.pregunta, [Validators.required]),
            tipoPregunta: new FormControl(duplicate.tipoPregunta, [Validators.required]),
            respuesta   : new FormArray([])
        })
        if(duplicate.tipoPregunta != 1) {
            duplicate.respuesta.forEach(element => {
                (newPregunta.get('respuesta') as FormArray)
                    .push(new FormGroup({
                        respuestaID: new FormControl(0),
                        opcion     : new FormControl(element.opcion, [Validators.required]),
                    }))
                });
        } else {
            (newPregunta.get('respuesta') as FormArray)
                        .push(new FormControl(null))
        }
        
        this.preguntasForm.push(newPregunta)
    }

    drop(event: CdkDragDrop<any[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
            transferArrayItem(event.previousContainer.data,
                            event.container.data,
                            event.previousIndex,
                            event.currentIndex);
        }
        this.preguntasForm.updateValueAndValidity()
    }

    async enviarFormulario() {
        const encuesta: IEncuesta = this.createEncuesta();
        encuesta.preguntas = this.populatePreguntas();

        const {
            Identificador,
            Descripcion
        } = await this._encuestaService.registrarEncuesta(encuesta);

        if(Identificador === "success"){
            return this._route.navigate(["Atencion/Encuesta"])
        }

        return __messageSnackBar(this._matSnackBar, Descripcion)
    }

    createEncuesta(): IEncuesta {
        const [fechaIni, fechaFin] = _orderRangeDate(this.fechaForm) 
        
        const newEncuesta =  {
            encuestaID   : 0,
            titulo       : this.tituloForm,
            fechaRegistro: moment().format('yyyy-MM-DD'),
            fechaInicio  : fechaIni,
            fechaFin     : fechaFin,
            preguntas    : []
        }

        return newEncuesta;
    }

    populatePreguntas(): IPregunta[] {
        const newPreguntas = []
        const valuePreg = this.preguntasForm.value;

        for (const key in valuePreg) {
            const pregunta: IPregunta = {
                preguntaEncuestaID: Number(valuePreg[key].preguntaId) || Number(key),
                tipo              : Number(valuePreg[key].tipoPregunta),
                pregunta          : valuePreg[key].pregunta,
                orden             : Number(key),
                respuesta         : []
            }
            if(pregunta.tipo !== 1)
                this.populateRespuesta(pregunta, valuePreg[key].respuesta);
            newPreguntas.push(pregunta);
        }

        return newPreguntas;
    }

    populateRespuesta(pregunta: IPregunta, respuestaArr: Array<any>) {
        for (const key in respuestaArr) {
            pregunta.respuesta.push({
                RespuestaDefinidaID: Number(respuestaArr[key].respuestaID) || 0,
                Opcion             : respuestaArr[key].opcion,
                Orden              : Number(key)
            })
        }
    }

    get preguntasForm() {
        return (this.formEncuesta.get('preguntas') as FormArray)
    }
    get fechaForm() {
        return this.formEncuesta.get('fecha').value
    }
    get tituloForm() {
        return this.formEncuesta.get('titulo').value;
    }
}

export interface IEncuesta {
    encuestaID   : number;
    titulo       : string;
    fechaRegistro: string;
    fechaInicio  : string;
    fechaFin     : string;
    preguntas    : IPregunta[]
}
export interface IPregunta {
    preguntaEncuestaID?: number;
    pregunta           : string;
    tipo               : number;
    orden              : number;
    respuesta          : Array<IRespuesta>;
}

export interface IRespuesta {
    RespuestaDefinidaID: number;
    Opcion             : string;
    Orden              : number;
}