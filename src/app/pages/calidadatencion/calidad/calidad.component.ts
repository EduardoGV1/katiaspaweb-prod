import { Component, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ServicioService } from "src/app/shared/services/servicio.service";
import { __messageSnackBar } from "src/app/utils/helpers";

@Component({
    selector: 'app-calidad-atencion-component',
    templateUrl: './calidad.component.html',
    styleUrls: ['./calidad.component.scss']
})
export class CalidadComponent implements OnInit {
    pagInicio: number = 0;
    cantidadInicio: number = 100;

    reportes: [] = [];
    puntiacionServicios: [] = [];
    puntuacionTrabajadores: [] = [];

    puntuacionxPersona: [] = [];
    puntuacionxPersonaTrabajador: [] = [];
    puntuacionNPS: number;
    colorLabelNPS: String;

    

    //Seccion servicios
    displayedColumnsReportes: string[] = ['FechaInicio', 'FechaFin', 'Detalle'];
    displayedColumnsPuntuacionServicios: string[] = ['Servicio', 'NPS', 'Detalle'];
    displayedColumnsPuntuacionServiciosxCliente: string[] = ['Cliente', 'Fecha', 'Puntuacion'];
    //Seccion Trabajadores
    displayedColumnsPuntuacionTrabajadores :string[]=['Nombre','Puntuacion','Detalle']
    displayedColumnsPuntuacionTrabajadoresxCliente :string[]=['Nombre','Fecha','Puntuacion']


    constructor(
        private _serviceServicio: ServicioService,
        private _matSnackBar: MatSnackBar
    ) { }
    ngOnInit(): void {
        this.listarReportePaginado();
    }

    async listarReportePaginado() {

        try {
            const { Identificador, Mensaje, Descripcion, Reportes, PuntuacionTrabajadores, Total } = await this._serviceServicio.listarReporteCalificacionPaginado({ pagina: this.pagInicio, cantidad: this.cantidadInicio })
            console.table(Reportes)
            console.log(Reportes)
            console.table(PuntuacionTrabajadores)
            this.reportes = Reportes;
            //this.puntiacionServicios=Reportes.PuntuacionServicios
            //this.puntuacionTrabajadores=Reportes.PuntuacionTrabajadores
        } catch (error) {
            __messageSnackBar(this._matSnackBar, "Error al obtener el reporte");
        }
    }

    showDetail(item: any) {
        console.log(item)
        this.puntiacionServicios = [];
        this.puntuacionxPersona = []
        this.puntuacionxPersonaTrabajador=[];
        this.puntiacionServicios = item.PuntuacionServicios
        this.puntuacionTrabajadores= item.PuntuacionTrabajadores
        this.puntuacionNPS = item.NPS
        this.colorLabelNPS = this.getColor(this.puntuacionNPS);
        //
    }

    showDetailPersonas(item: any) {
        this.puntuacionxPersona = [];
        this.puntuacionxPersona = item.Puntuaciones;
    }
    showDetailPersonasxTrabajadores(item:any){
        this.puntuacionxPersonaTrabajador=[];
        this.puntuacionxPersonaTrabajador=item.Puntuaciones
    }

    getColor(nivelNps: number): String {
        var color: String;
        color = (nivelNps < 0) ? '#ec3903' : color;
        color = (nivelNps <= 40) ? '#6b8106' : color;
        color = (nivelNps < 0) ? '#01a001' : color;
        return color;
    }
}