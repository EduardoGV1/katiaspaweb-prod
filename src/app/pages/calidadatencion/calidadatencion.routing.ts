import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PagesComponent } from "../pages.component";
import { CalidadComponent } from "./calidad/calidad.component";

const routes: Routes = [
    {
        path:'',
        component:PagesComponent,
        children:[
            {
                path:'Calidad',
                component:CalidadComponent,
                data:{breadcrumb:"Gestion Calidad"}
            }
        ],
        data:{breadcrumb:"Calidad"}
    }
];

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class CalidadAtencionRoutingModule{}