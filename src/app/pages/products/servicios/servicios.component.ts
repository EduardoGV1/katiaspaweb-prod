import { Component, OnInit, ViewChild } from '@angular/core';
import { ServicioService } from '../../../shared/services/servicio.service';
import { IApiResponse } from '../../../shared/models/response';
import { __messageSnackBar } from '../../../utils/helpers';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AgregarServiciosModalComponent } from './agregar-servicios/agregar-servicios.component';
import { EliminarServiciosModalComponent } from './eliminar-servicios/eliminar-servicios.component';
import { VerDetalleServiciosModalComponent } from './verDetalle-servicios/verDetalle-servicios.component';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ObvsService } from 'src/app/shared/services/obvs.service';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html'
})
export class ServiciosComponent implements OnInit {

  serviciosArr  = []
  categoriasArr = []
  filtrosForm: FormGroup
  paginatorPageSize = 10;
  totalRecords=0;

  @ViewChild(MatPaginator) paginator: MatPaginator;
    displayedColumns: string[] = ['ServicioNombre','CategoriaNombre','ServicioPrecio','ServicioDuracion','Genero','Acción'];
    pageEvent: PageEvent;

  constructor(
    private _servicioService: ServicioService,
    private _matSnackBar    : MatSnackBar,
    public  dialog          : MatDialog,
    public _obvsService: ObvsService,
  ) { 
    this.filtrosForm = new FormGroup({
      categoria: new FormControl(""),
      isActivo : new FormControl(""),
      genero   : new FormControl("")
    })
  }

  async listarCategoriaServicosDropdown() {
    const {
      Descripcion,
      Identificador,
      Resultado
    }: IApiResponse = await this._servicioService.listarCategoriaServiciosDropdown();
    if(Identificador == "error") {
      return __messageSnackBar(this._matSnackBar, Descripcion)
    }
    this.categoriasArr = Resultado
  }

  async listarServicios(filtros?:any) {
    debugger
    var filtro ={
        isActivo           : 1,
        genero             : null,
        pagina             : 1,
        cantidad           : 10,
        categoriaServicioID: 0
    }
    if (filtros) {
      filtro.isActivo=(filtros.isActivo && filtros.isActivo!="")?Number(filtros.isActivo):2;
      filtro.genero = (filtros.genero && filtros.genero!="")?filtros.genero:null;
      filtro.categoriaServicioID = (filtros.categoriaServicioID && filtros.categoriaServicioID!="")?Number(filtros.categoriaServicioID):0;
    }
    try {
      this._obvsService.toogleSpinner()
      const {
        Descripcion,
        Identificador,
        Resultado,
        Total
      }: IApiResponse = await this._servicioService.listarServiciosPaginado(filtro)

      if(Identificador == "error") {
        return __messageSnackBar(this._matSnackBar, Descripcion)
      }
      this.serviciosArr = Resultado
      this.totalRecords=Total
    } catch (error) {
      console.log(error)
    }
    finally{
      this._obvsService.toogleSpinner()
    }
  }

  async ngOnInit() {
    await this.listarServicios();
    await this.listarCategoriaServicosDropdown();
  }

  async paginado(event:any){
    console.log("event",event)
    //event.pageIndex*this.paginatorPageSize,this.paginatorPageSize
    let filtro ={
      isActivo           : 1,
      genero             : null,
      pagina             : event.pageIndex,
      cantidad           : this.paginatorPageSize,
      categoriaServicioID: 0
  }
  await this._servicioService.listarServiciosPaginado(filtro)

}

  agregarServicio(servicioId = null) {
    const dialogRef = this.dialog.open(AgregarServiciosModalComponent, {
      data        : servicioId,
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(() => {
      if(dialogRef.componentInstance.changeData) this.ngOnInit()
    })
  }

  eliminarServicio(servicioId) {
    const dialogRef = this.dialog.open(EliminarServiciosModalComponent, {
      data        : servicioId,
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(async () => {
      if(dialogRef.componentInstance.hasChange) {
        await this.listarServicios();
      }
    })
  }
  verDetalleServicio(servicioId = null){
    const dialogRef = this.dialog.open(VerDetalleServiciosModalComponent, {
      data: servicioId,
      disableClose:true
    })
    
  }

  buscarServicios(){
    this.listarServicios({"isActivo":this.filtrosForm.value.isActivo,"genero":this.filtrosForm.value.genero,"categoriaServicioID":this.filtrosForm.value.categoria});
  }

}
