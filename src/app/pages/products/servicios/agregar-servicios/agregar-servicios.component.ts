import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ServicioService } from '../../../../shared/services/servicio.service';
import { IApiResponse, INewServicio } from '../../../../shared/models/response';
import { MatSnackBar } from '@angular/material/snack-bar';
import { __messageSnackBar } from '../../../../utils/helpers';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
    selector: 'agregar-servicios',
    templateUrl: './agregar-servicios.component.html'
})
export class AgregarServiciosModalComponent implements OnInit {

    categoriasArr = []
    formularioGroup: FormGroup
    changeData     : boolean = false
    importe:number=0;
    montoIgv:number=0;
    total:Number=0;

    constructor(
        private _servicioService: ServicioService,
        private _matSnackBar    : MatSnackBar,
        public  dialogRef       : MatDialogRef<AgregarServiciosModalComponent>,
        private http            : HttpClient,
        private firestore       : AngularFirestore,
        @Inject(MAT_DIALOG_DATA) public servicioId: any
    ) {
        this.formularioGroup = new FormGroup({
            nombre     : new FormControl(null, [Validators.required]),
            descripcion: new FormControl(null, [Validators.required]),
            foto       : new FormControl(null, [Validators.required]),
            precio     : new FormControl(null, [Validators.required]),
            duracion   : new FormControl(null, [Validators.required]),
            servicio   : new FormControl("2" , [Validators.required]),
            descanso   : new FormControl(null, [Validators.required]),
            sesiones   : new FormControl(null, [Validators.required]),
            categoria  : new FormControl( "" , [Validators.required]),
            isActive   : new FormControl("1" , [Validators.required]),
            // isCuota    : new FormControl("0" , [Validators.required])
        })
    }

    addFotos(fotosArr) {
        this.formularioGroup.get('foto').setValue(fotosArr)
    }

    async getFotosFB(fotosFB: string[]) {
        const arrayFB = []
        fotosFB.map(async fUrl => {
            console.log(fUrl)
            this.firestore.collection('servicios').get().subscribe(value => console.log(value))
            const a = await this.http.get(fUrl);
            console.log(a)
        })
        console.log(arrayFB)
    }

    async ngOnInit() {
        try {
            this.formularioGroup.disable();
            if(this.servicioId) {
                const {
                    Identificador,
                    Resultado,
                    Descripcion
                }: IApiResponse = await this._servicioService.obtenerServicioPorIDWeb(this.servicioId);
                if(Identificador === "sucess") {
                    this.formularioGroup.patchValue({
                        nombre     : Resultado.Nombre,
                        descripcion: Resultado?.Descripcion,
                        precio     : Resultado?.DetalleServicio?.Precio,
                        duracion   : Resultado?.DetalleServicio?.Duracion,
                        servicio   : Resultado?.DetalleServicio?.Genero,
                        descanso   : Resultado?.DetalleServicio?.TiempoDescanso,
                        sesiones   : Resultado?.DetalleServicio?.NroSesionesAprox,
                        categoria  : Resultado?.CategoriaServicio?.CategoriaServicioID,
                        isActive   : Resultado?.IsActivo  ? "1" : "0",
                        // isCuota    : Resultado?.DetalleServicio?.PagoCuota ? "1" : "0",
                    })
                    this.importe=Resultado?.DetalleServicio.Importe,
                    this.montoIgv=Resultado?.DetalleServicio.IGV,
                    await this.getFotosFB(Resultado.UrlFotoss)
                }
                else {
                    this.dialogRef.close();
                    return __messageSnackBar(this._matSnackBar, Descripcion)
                }
            }
            const {
                Identificador,
                Resultado,
                Descripcion
            }: IApiResponse = await this._servicioService.listarCategoriaServiciosDropdown();
            if(Identificador === 'sucess')
                this.categoriasArr = Resultado
            else {
                this.dialogRef.close();
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
        } catch (error) {
            console.log(error)
        } finally {
            this.formularioGroup.enable();
        }
    }

    async registrarProducto() {
        try {
            this.formularioGroup.disable();
            const isActive = (this.formularioGroup.get('isActive').value === "1");
            // const isCuota  = (this.formularioGroup.get('isCuota').value === "1");
            const isCuota  = false

            // if(Number(this.formularioGroup.get('sesiones').value) <= 1
            //     && isCuota) {
            //         return __messageSnackBar(this._matSnackBar, "Para poder activar el pago por Cuotas debe tener como mínimo más de una sesión")
            // }

            const fotosArr = [...this.formularioGroup.get('foto').value];

            const fotosSend = fotosArr.map(f => f.path.split(',')[1]);

            const data: INewServicio = {
                nombre           : this.formularioGroup.get('nombre').value,
                descripcion      : this.formularioGroup.get('descripcion').value,
                foto             : fotosSend.toString(),
                isActivo         : isActive,
                categoriaServicio: {
                    categoriaServicioID: Number(this.formularioGroup.get('categoria').value)
                },
                detalleServicio: {
                    duracion        : Number(this.formularioGroup.get('duracion').value),
                    genero          : this.formularioGroup.get('servicio').value,
                    nroSesionesAprox: Number(this.formularioGroup.get('sesiones').value),
                    precio          : Number(this.formularioGroup.get('precio').value),
                    tiempoDescanso  : Number(this.formularioGroup.get('descanso').value),
                    pagoCuota       : isCuota,
                    importe         : this.importe,
                    igv             : this.montoIgv,
                }
            }
            !!this.servicioId ? await this.editarProducto(data)
                                : await this.nuevoProducto(data);
        } catch (error) {
            console.log(error)
        } finally {
            this.formularioGroup.enable()
        }
    }

    changeMonto(event:any){
        let montoPrecio:number =+event;
        console.log(montoPrecio)
        this.importe=Number((montoPrecio/1.18).toFixed(2))
        this.montoIgv=Number((montoPrecio-this.importe).toFixed(2));
        this.total=this.importe+this.montoIgv;

    }

    async nuevoProducto(data: INewServicio) {
        const {
            Descripcion,
            Identificador
        }: IApiResponse = await this._servicioService.registrarServicio(data);

        if(Identificador === "sucess") {
            this.changeData = true
            this.dialogRef.close()
        }
        
        __messageSnackBar(this._matSnackBar, Descripcion)
    }

    async editarProducto(data: INewServicio) {
        data.servicioID = this.servicioId;

        const {
            Descripcion,
            Identificador
        }: IApiResponse = await this._servicioService.editarServicio(data);

        if(Identificador === "sucess") {
            this.changeData = true
            this.dialogRef.close()
        }
        
        __messageSnackBar(this._matSnackBar, Descripcion)
    }
}