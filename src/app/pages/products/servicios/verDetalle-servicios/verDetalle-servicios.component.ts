import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ServicioService } from '../../../../shared/services/servicio.service';
import { IApiResponse, INewServicio } from '../../../../shared/models/response';
import { MatSnackBar } from '@angular/material/snack-bar';
import { __messageSnackBar } from '../../../../utils/helpers';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
    selector: 'verDetalle-servicios',
    templateUrl: './verDetalle-servicios.component.html'
})
export class VerDetalleServiciosModalComponent implements OnInit {

    categoriasArr = []
    formularioGroup: FormGroup
    changeData     : boolean = false
    importe:number=0;
    montoIgv:number=0;

    constructor(
        private _servicioService: ServicioService,
        private _matSnackBar    : MatSnackBar,
        public  dialogRef       : MatDialogRef<VerDetalleServiciosModalComponent>,
        private http            : HttpClient,
        private firestore       : AngularFirestore,
        @Inject(MAT_DIALOG_DATA) public servicioId: any
    ) {
        this.formularioGroup = new FormGroup({
            nombre     : new FormControl({value:null,disabled:true}, [Validators.required]),
            descripcion: new FormControl({value:null,disabled:false}, [Validators.required]),
            foto       : new FormControl(null, [Validators.required]),
            precio     : new FormControl(null, [Validators.required]),
            duracion   : new FormControl(null, [Validators.required]),
            servicio   : new FormControl("2" , [Validators.required]),
            descanso   : new FormControl(null, [Validators.required]),
            sesiones   : new FormControl(null, [Validators.required]),
            categoria  : new FormControl( "" , [Validators.required]),
            isActive   : new FormControl("1" , [Validators.required]),
            // isCuota    : new FormControl("0" , [Validators.required])
        })
    }

    addFotos(fotosArr) {
        this.formularioGroup.get('foto').setValue(fotosArr)
    }

    async getFotosFB(fotosFB: string[]) {
        const arrayFB = []
        fotosFB.map(async fUrl => {
            console.log(fUrl)
            arrayFB.push(fUrl)
            this.firestore.collection('servicios').get().subscribe(value => console.log(value))
            const a = await this.http.get(fUrl);
            console.log(a)
        })
        console.log(arrayFB)
    }

    async ngOnInit() {
        try {
            this.formularioGroup.disable();
            if(this.servicioId) {
                const {
                    Identificador,
                    Resultado,
                    Descripcion
                }: IApiResponse = await this._servicioService.obtenerServicioPorIDWeb(this.servicioId);
                if(Identificador === "sucess") {
                    this.formularioGroup.patchValue({
                        nombre     : Resultado.Nombre,
                        descripcion: Resultado?.Descripcion,
                        precio     : Resultado?.DetalleServicio?.Precio,
                        duracion   : Resultado?.DetalleServicio?.Duracion,
                        servicio   : Resultado?.DetalleServicio?.Genero,
                        descanso   : Resultado?.DetalleServicio?.TiempoDescanso,
                        sesiones   : Resultado?.DetalleServicio?.NroSesionesAprox,
                        categoria  : Resultado?.CategoriaServicio?.CategoriaServicioID,
                        isActive   : Resultado?.IsActivo  ? "1" : "0",
                        // isCuota    : Resultado?.DetalleServicio?.PagoCuota ? "1" : "0",
                    })
                    this.importe=Resultado?.DetalleServicio.Importe,
                    this.montoIgv=Resultado?.DetalleServicio.IGV,
                    await this.getFotosFB(Resultado.UrlFotoss)
                }
                else {
                    this.dialogRef.close();
                    return __messageSnackBar(this._matSnackBar, Descripcion)
                }
            }
            const {
                Identificador,
                Resultado,
                Descripcion
            }: IApiResponse = await this._servicioService.listarCategoriaServiciosDropdown();
            if(Identificador === 'sucess')
                this.categoriasArr = Resultado
            else {
                this.dialogRef.close();
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
        } catch (error) {
            console.log(error)
        } finally {
            //this.formularioGroup.enable();
        }
    }
}