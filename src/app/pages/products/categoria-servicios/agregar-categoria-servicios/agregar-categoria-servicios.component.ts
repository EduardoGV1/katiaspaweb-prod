import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ServicioService } from '../../../../shared/services/servicio.service';
import { IApiResponse } from '../../../../shared/models/response';
import { MatSnackBar } from '@angular/material/snack-bar';
import { __messageSnackBar } from '../../../../utils/helpers';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'agregar-categoria-servicios',
    templateUrl: './agregar-categoria-servicios.component.html'
})
export class AgregarCategoriaServiciosModalComponent implements OnInit{

    formularioGroup: FormGroup
    categoriasArr=[]
    changeData=false

    constructor(
        private _servicioService: ServicioService,
        private _matSnackBar :    MatSnackBar,
        public dialogRef : MatDialogRef<AgregarCategoriaServiciosModalComponent>
    ){
        this.formularioGroup=new FormGroup({
            descripcion: new FormControl(null,[Validators.required])
        })
    }
    async registrarCategoriaServicios(){
        try{
            this.formularioGroup.disable();
            const{
                Descripcion,
                Identificador,
                Mensaje,
                Resultado
            }: IApiResponse = await this._servicioService.insertCategoriaServicio(
                this.formularioGroup.get('descripcion').value

            )
            if(Identificador === "success") {
                this.dialogRef.close()
                this.changeData=true
            
            }
            return __messageSnackBar(this._matSnackBar, Descripcion)

        } catch(error){
            console.log(error)
        } finally{
            this.formularioGroup.enable();
        }

    }
    async ngOnInit() {
        
    }
    
}