import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Component
import { SharedModule } from '../../shared/shared.module';
import { CategoriaServiciosComponent } from './categoria-servicios/categoria-servicios.component';
import { ConsultorioComponent } from './consultorio/consultorio.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { ProductRoutingModule } from './product.routing';
import { PagesModule } from '../pages.module';
import { AgregarServiciosModalComponent } from './servicios/agregar-servicios/agregar-servicios.component';
import { AgregarCategoriaServiciosModalComponent } from './categoria-servicios/agregar-categoria-servicios/agregar-categoria-servicios.component';
import { EliminarServiciosModalComponent } from './servicios/eliminar-servicios/eliminar-servicios.component';
import { EditarCategoriaServiciosModalComponent } from './categoria-servicios/editar-categorias-servicios/editar-categoria-servicios.component';
import { AgregarConsultorioModalComponent } from './consultorio/agregar-consulorio/agregar-consultorio.component';
import { ConsultarDetalleModalComponent } from './consultorio/detalle-consultorio/detalle-consultorio.component';
import { VerDetalleServiciosModalComponent } from './servicios/verDetalle-servicios/verDetalle-servicios.component';





@NgModule({
  declarations: [
    CategoriaServiciosComponent,
    ConsultorioComponent,
    ServiciosComponent,
    AgregarServiciosModalComponent,
    EliminarServiciosModalComponent,
    AgregarConsultorioModalComponent,
    AgregarCategoriaServiciosModalComponent,
    EliminarServiciosModalComponent,
    EditarCategoriaServiciosModalComponent,
    ConsultarDetalleModalComponent,
    VerDetalleServiciosModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PagesModule,
    ProductRoutingModule
  ],
  
})
export class ProductModule { }
