import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { IApiResponse } from "src/app/shared/models/response";
import { ConsultasService } from "src/app/shared/services/consulta.service";
import { ObvsService } from "src/app/shared/services/obvs.service";
import { __messageSnackBar } from "src/app/utils/helpers";
import { ConsultorioComponent } from "../consultorio.component";


@Component({
    selector: 'detalle-consultorio',
    templateUrl: './detalle-consultorio.component.html'
})
export class ConsultarDetalleModalComponent implements OnInit {
    serviciosAsignados: IServicio[] = []
    serviciosDisponibles : IServicio[]=[];
    serviciosListado : IServicio[]=[];
    constructor(
        private _consultaService: ConsultasService,
        private _obvsService: ObvsService,
        private _matSnackBar: MatSnackBar,
        public dialogRef: MatDialogRef<ConsultarDetalleModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {

    }
    async ngOnInit() {
        this._obvsService.toogleSpinner()
        await this.listarServiciosAsignadosConsultorio();
        await this.listarServicioDisponiblesConsultorio();
        await this.validateEliminarServicio();
        // const lista= this.serviciosArr.filter((item,index)=>{
        //     return this.serviciosArr.indexOf(item)===index;
        // })
        // this.serviciosArr=lista;
        this._obvsService.toogleSpinner()
    }
    async listarServiciosAsignadosConsultorio() {
        try {
            const {
                Descripcion,
                Identificador,
                Resultado
            }: IApiResponse = await this._consultaService.listarServiciosRegistradoPorConsultorio({
                ConsultorioID: this.data
            })
            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
            this.serviciosAsignados=Resultado;
            
            this.serviciosAsignados.forEach(service => {
                service['asignado']=true
                if (service.IsActivo) {
                    service['checked']=true
                }
            });
        } catch (error) {
            console.log(error)
        }
    }
    async listarServicioDisponiblesConsultorio() {
        try {
            const {
                Descripcion,
                Identificador,
                Resultado
            }: IApiResponse = await this._consultaService.listarServiciosDisponiblesPorConsultorioId({
                consultorioID: this.data
            })
            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
            this.serviciosDisponibles = Resultado
            this.serviciosDisponibles.map(a => {
                let seElimina = !!Resultado.find(e => a.ServicioID == e.ServicioID);
                a.PuedeEliminar = seElimina
                console.log(a)
                // console.log(Resultado[0])
            })
            // Resultado.forEach(service => this.serviciosArr.push({
            //     servicioID: service.ServicioID,
            //     nombre: service.Nombre,
            //     asignado: false,
            //     checked: false
            // })
            // )
        } catch (error) {
            console.log(error)
        }
    }

    async validateEliminarServicio(){
        var serviciosConcatenados = this.serviciosAsignados.concat(this.serviciosDisponibles);
        // this.serviciosListado.concat(this.serviciosDisponibles);
        console.log("this.serviciosListado",serviciosConcatenados)
        this.serviciosListado = serviciosConcatenados.filter((obj,index)=> {
            return index === serviciosConcatenados.findIndex(o=> obj.ServicioID===o.ServicioID);
        })
        this.serviciosListado.forEach(service => {
            if (this.serviciosAsignados.find((item)=> item.ServicioID==service.ServicioID)) {
                service['asignado']=true
                if (service.IsActivo) {
                    service['checked']=true
                }
            } else {
                service['asignado']=false
                    service['checked']=false
            }
            // this.serviciosAsignados.forEach(servicioAsignado => {
            //     if (service.ServicioID==servicioAsignado.ServicioID) {
            //         service['asignado']=true
            //         service['checked']=true
            //     } else {
            //         service['asignado']=false
            //         service['checked']=false
            //     }
            // })
        });
        console.log("this.vr2",this.serviciosListado)

        // try {
        //     const {
        //         Descripcion,
        //         Identificador,
        //         Resultado
        //     }: IApiResponse = await this._consultaService.listarServiciosRegistradoPorConsultorio({
        //         ConsultorioID: this.data
        //     })
        //     if (Identificador == "error") {
        //         return __messageSnackBar(this._matSnackBar, Descripcion)
        //     }
        //     this.serviciosDisponibles.map(a => {
        //         let seElimina = !!Resultado.find(e => a.ServicioID == e.ServicioID);
        //         a.seElimina = seElimina
        //         console.log(a)
        //         console.log(Resultado[0])
        //     })
        //     console.log(Resultado,this.serviciosDisponibles)
        // } catch (error) {
        //     console.log(error)
        // }
    }

    changeValue(evt, index) {
        console.log('EVT',evt, 'INDEX', index);
        const checked = evt.target.checked;
        this.serviciosListado[index].checked = checked
    }

    async activarInactivar(element) {
        let resp:any;
        if (element.asignado && !element.checked)
            await this._consultaService.desactivarServicio({
                ConsultorioID: this.data,
                ServicioID: element.ServicioID
            })
        if (element.asignado && element.checked)
           resp= await this._consultaService.activarServicio({
            ConsultorioID: this.data,
                ServicioID: element.ServicioID
            })
            console.log('BODY',{
                ConsultorioID: this.data,
                ServicioID: element.ServicioID
            });
            console.log('RESP',resp);
    }
    async agregarQuitar (element) {
        //debugger;
        let servicioID = element.ServicioID;
        let servicio = this.serviciosListado.filter(item => item.ServicioID==servicioID)
        if (!element.asignado && element.checked)
            await this._consultaService.asignarServicio({
                ConsultorioID: this.data,
                ServicioID: element.ServicioID,

            })
        if (element.asignado && !element.checked)
            await this._consultaService.quitarServicio({
                ConsultorioID: this.data,
                ServicioID: element.ServicioID,

            })
    }

    async actualizarDetalle() {
        //debugger;
        try {
            this._obvsService.toogleSpinner()
            this.serviciosListado.forEach(async (element) => {
                if (element.PuedeEliminar) {
                    this.agregarQuitar(element)
                } else {
                    this.activarInactivar(element)
                }
            })
            __messageSnackBar(this._matSnackBar, "Se actualizó exitosamente")
            this.dialogRef.close()
        } catch (err) {
            console.log(err)
        } finally {
            this._obvsService.toogleSpinner()
        }
    }
}

export interface IServicio {
    ServicioID: number;
    Nombre: string;
    asignado: boolean;
    checked: boolean;
    PuedeEliminar ?: boolean;
    IsActivo?:boolean;
}