import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from '../pages.component';
import { CategoriaServiciosComponent } from '../products/categoria-servicios/categoria-servicios.component';
import { ConsultorioComponent } from '../products/consultorio/consultorio.component';
import { ServiciosComponent } from './servicios/servicios.component';

const routes: Routes = [
    {
        path     : '',
        component: PagesComponent,
        children : [
            { 
                path: 'CategoriaServicios',
                component: CategoriaServiciosComponent ,
                data: {breadcrumb: "Categoria Servicios"}
            },
            {
                path: 'Consultorios',
                component: ConsultorioComponent,
                data: {breadcrumb: "Consultorios"}
            },
            { 
                path: 'Servicios',
                component: ServiciosComponent ,
                data: {breadcrumb: "Servicios"}
            }
        ],
        data: {breadcrumb: "Gestion de Servicios"}
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductRoutingModule { }
