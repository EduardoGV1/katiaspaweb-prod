import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from '../pages.component';
import { PersonalComponent } from './personal/personal.component';
import { EditarPersonalComponent } from './personal/editar/editar-personal.component';
import { ClientesComponent } from './clientes/clientes.component';
import { DetalleClientesComponent } from './clientes/detalle-clientes/detalle-clientes.component';
import { HorariosPersonalComponent } from './personal/horarios/horarios-personal.component';

const routes: Routes = [
    {
        path     : '',
        component: PagesComponent,
        children : [
            { 
                path    : 'Personal',
                data    : {breadcrumb: "Personal"},
                children: [
                    {
                        path     : "",
                        component: PersonalComponent,
                    },
                    {
                        path     : "editar/:id",
                        component: EditarPersonalComponent,
                        data     : {breadcrumb: "Editar"}
                    },
                    {
                        path     : "horarios/:id",
                        component: HorariosPersonalComponent,
                        data     : {breadcrumb: "Horarios"}
                    },
                    {
                        path     : "horarios",
                        component: HorariosPersonalComponent,
                        data     : {breadcrumb: "Horarios"}
                    }
                ]
            },
            { 
                path     : 'Cliente',
                component: ClientesComponent ,
                data     : {breadcrumb: "Administrar Clientes"},
            },
            {
                path     : 'VerCliente/:id',
                component: DetalleClientesComponent
            }
        ],
        data: {breadcrumb: "Gestion de Clientes y Personal"}
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PersonsRoutingModule { }
