import { DatePipe } from "@angular/common";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import * as moment from "moment";
import { IApiResponse } from "src/app/shared/models/response";
import { ClienteService } from "src/app/shared/services/cliente.service";
import { __messageSnackBar } from "src/app/utils/helpers";
const ToDay=moment(new Date()).add(-18,'years').toDate()
@Component({
    selector:'app-agregarCliente',
    templateUrl:'./agregar-clientes.component.html'
})
export class AgregarClienteComponent implements OnInit{
    changeData = false
    clientesAddForm: FormGroup
    @ViewChild('endDatePicker') endDatePicker!: any;

    disabledEndDate = (startValue: Date): boolean => {
        if (!startValue || !ToDay) {
          return false;
        }
        return startValue.getTime() > ToDay.getTime();
      };

    constructor(
        private _clienteService: ClienteService,
        private _matSnackBar: MatSnackBar,
        public datePipe: DatePipe,
        public dialogRef:MatDialogRef<AgregarClienteComponent>
    ){
        this.clientesAddForm = new FormGroup({
            nombre: new FormControl(null,[Validators.required,Validators.min(1000)]),
            fecha:new FormControl(null,[Validators.required]),
            apellido: new FormControl(null, [Validators.required]),
            dni: new FormControl(null, [Validators.required]),
            telefono: new FormControl(null, [Validators.required,Validators.minLength(8)]),
            correo: new FormControl(null, [Validators.required,Validators.email]),
            genero: new FormControl(null, [Validators.required]),
            direccion: new FormControl(null, [Validators.required]),
            respuestaEncuesta: new FormControl(),
        })
}
    async ngOnInit() {
        
    }
    markFormGroupTouched(formGroup: FormGroup) {
        Object.values(formGroup.controls).forEach(control => {
          control.markAsTouched();
      
          if (control instanceof FormGroup) {
            this.markFormGroupTouched(control);
          }
        });
    }

    async registrarCliente(){
        if (this.clientesAddForm.valid) {
            console.log("valido")
        }else{
            console.log("no valido")
            return this.markFormGroupTouched(this.clientesAddForm)
        }

        const _genero = (this.clientesAddForm.value.genero == 'true')?true:false
        console.log(this.clientesAddForm)
        console.log(this.clientesAddForm.value)
        this.clientesAddForm.disable();
        const fecha = this.datePipe.transform(new Date(), 'yyyy-MM-dd hh:mm:ss')
        const fechaNacString = this.datePipe.transform(this.clientesAddForm.value.fecha,'yyyy-MM-dd')
        try {
            const {
                Mensaje,
                Identificador,
                Descripcion
            } : IApiResponse = await this._clienteService.agregarCliente({
                nombres: this.clientesAddForm.value.nombre,
                apellidos:this.clientesAddForm.value.apellido,
                numeroDocumento:this.clientesAddForm.value.dni,
                telefono:Number(this.clientesAddForm.value.telefono),
                correo:this.clientesAddForm.value.correo,
                direccion:this.clientesAddForm.value.direccion,
                fechaNacString: fechaNacString,
                //fechaNacString:new Date(this.clientesAddForm.value.fecha).toLocaleDateString(),
                genero:_genero,
                nombreFull:this.clientesAddForm.value.nombre+" "+this.clientesAddForm.value.apellido,
                password:"admin123",
                fechaRegistro:new Date(),
                fechaRegistroString:fecha
            })
            if (Identificador==="success") {
                this.dialogRef.close();
                this.changeData=true;
                this.clientesAddForm.enable();
            }
            return __messageSnackBar(this._matSnackBar,Descripcion)
        } catch (error) {
            return __messageSnackBar(this._matSnackBar,"Error al guardar cliente")
        } finally {
            this.clientesAddForm.enable();
        }
    }
}