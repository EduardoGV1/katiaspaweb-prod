
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { IApiResponse } from 'src/app/shared/models/response';
import { PersonsService } from 'src/app/shared/services/persons.service';
import { __messageSnackBar } from 'src/app/utils/helpers';
import { EditarHorarioModalComponent } from './editar-horarios/editar-horarios.component';
@Component({
    selector:'app-horarios-personal',
    templateUrl:'./horarios-personal.component.html'
})
export class HorariosPersonalComponent implements OnInit {
    id:any | null;
    listaHorarios:[];
    listaHorariosAll:[];

    constructor(
        private route : ActivatedRoute,
        private _personsService: PersonsService,
        private _matSnackBar: MatSnackBar,
        private dialog         : MatDialog,
    ){
        
    }
    ngOnInit(): void {
        //throw new Error('Method not implemented.');
        this.id = this.route.snapshot.paramMap.get('id')
        console.log("id de la ruta:",this.id)
        if (this.id == null) {
            this.cargarHorariosAll();
        } else{
            this.cargarHorariosById(+this.id);
        }

    }

    async cargarHorariosById(id:number){
        console.log("ID:",id)
        try {
            const {
                Identificador,
                Mensaje,
                Descripcion,
                Resultado
                //_personsService
            }:IApiResponse = await this._personsService.obtenerHorarioPorTrabajadorById({
                usuarioID:id
            })
            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar,Descripcion)
            }
            console.log("Resultado:\n",Resultado)
            this.parseHorariosById(Resultado);
        } catch (error) {
            
        }
    }

    parseHorariosById(horarios:any){
        this.listaHorarios=horarios
    }

    horarioClick(data1:any,data2:any,data3:any){
        console.log("data",data1)
        console.log("data2",data2)
        console.log("data3",data3)
        console.log("data4",this.listaHorariosAll)
        this.dialog.open(EditarHorarioModalComponent,{
            disableClose:true,
            data: {data1,data2,data3,data4:this.listaHorariosAll}
        })
    }
    agregarHorario(){
        
    }

    parseHorariosAll(horarios:any){
        this.listaHorariosAll=horarios
        console.log("horarios ALLLLL:\n",horarios)
        console.log("listaHorariosAll ALLLLL:\n",this.listaHorariosAll)
    }


    async cargarHorariosAll(){
        try {
            const {
                Identificador,
                Mensaje,
                Descripcion,
                Resultado
                //_personsService
            }:IApiResponse = await this._personsService.obtenerHorarioPorTrabajadorAll()
            if (Identificador == "error") {
                return __messageSnackBar(this._matSnackBar,Descripcion)
            }
            console.log("Resultado ALLLLL:\n",Resultado)
            this.parseHorariosAll(Resultado);
        } catch (error) {
            
        }
        console.log("All")
    }
}