import { Component, Inject, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { IApiResponse } from "src/app/shared/models/response";
import { PersonsService } from "src/app/shared/services/persons.service";
import { __messageSnackBar } from '../../../../../utils/helpers';
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
    selector: 'app-editar-horarios',
    templateUrl: './editar-horarios.component.html'
})

export class EditarHorarioModalComponent implements OnInit {
    numberTimePicker: number
    horarios: []
    grupoHorarios: FormGroup
    constructor(
        public dialogRef: MatDialogRef<EditarHorarioModalComponent>,
        private _matSnackBar: MatSnackBar,
        private _personsService: PersonsService,
        private fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        console.log("data llego modal:::", data)
        this.numberTimePicker = data.data3.Turnos.length
        this.horarios = data.data3.Turnos
        console.log("this.numberTimePicker", this.numberTimePicker)
    }
    ngOnInit(): void {
        if (this.numberTimePicker == 0) {
            this.grupoHorarios = this.fb.group({
                horaInicioFin: this.fb.array([this.fb.group({ horaInicio: [Date()], horaFin: [Date()],validate:['✔️ | '] })])
            })
        } else {
            this.grupoHorarios = this.fb.group({
                horaInicioFin: this.fb.array([this.fb.group({ horaInicio: [this.data.data3.Turnos[0].HoraInicio], horaFin: [this.data.data3.Turnos[0].HoraFin],validate:['✔️ | '] })])
            })
            this.addturnosEntrada();
        }

    }
    addturnosEntrada() {
        for (let index = 1; index < this.numberTimePicker; index++) {
            // this.grupoHorarios = this.fb.group({
            //     horaInicioFin: this.fb.array([this.fb.group({ horaInicio: [this.data.data3.Turnos[index].HoraInicio], horaFin: [this.data.data3.Turnos[index].HoraFin] })])
            // })
            const control = <FormArray>this.grupoHorarios.controls['horaInicioFin'];
            control.push(this.fb.group({ horaInicio: [this.data.data3.Turnos[index].HoraInicio], horaFin: [this.data.data3.Turnos[index].HoraFin],validate:['✔️ | '] }))
        }
    }

    disableHours(): number[] {
        return [0, 1, 2, 3, 4, 5, 6, 7, 22, 23]
    }

    disableMinutes(): number[] {
        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59]
    }

    get getHorarios() {
        //console.log("this.grupoHorarios.get('horaInicioFin')"+this.grupoHorarios.get('horaInicioFin').value )
        return this.grupoHorarios.get('horaInicioFin') as FormArray;
    }

    async onSubmit(event: any) {

        const validacion= this.validarTurnos()
        console.log("validacion",validacion)
        //return true;

        console.log("event",event)
        var turnos=[];
        var request = []
        
        event.value.horaInicioFin.forEach(turno => {
            turnos.push(
                {
                    HoraInicio : new Date(turno.horaInicio),
                    HoraFin : new Date(turno.horaFin),
                }
            )
        });
        this.data.data4.forEach(trabajadorHorario => {
            if (trabajadorHorario.UsuarioID==this.data.data1.UsuarioID) {
                trabajadorHorario.usuarioHorario.Horario.forEach(dia => {
                    if (this.data.data3.Dia==dia.Dia) {
                        console.log("diaZZ",dia)
                        this.data.data3.Turnos = turnos
                    }
                    request.push(dia)
                });
            }
        });
        const aux1 = request.map(diaHorario =>{
            const rangoHorario =[]
            diaHorario.Turnos.forEach(item => {
                const turno = {
                    horaInicio:item.HoraInicio,
                    horaFin:item.HoraFin
                }
                rangoHorario.push(turno)
            });
            return{
                dia:diaHorario.Dia,
                turnos: rangoHorario
            }
        })
        const requestBody = {
            usuarioID:this.data.data1.UsuarioID,
            horario:aux1,
            isDefault:false
        }
        try {
            const {
                Identificador,
                Mensaje,
                Descripcion,
                Resultado
            }:IApiResponse = await this._personsService.registrarHorarioTrabajador(requestBody);
            if (Identificador== "error") {
                return __messageSnackBar(this._matSnackBar, Descripcion)
            }
            if (Identificador== "success") {
                __messageSnackBar(this._matSnackBar, Descripcion)
                this.dialogRef.close();

            }
        } catch (error) {
            console.log("Error",error)
            return __messageSnackBar(this._matSnackBar,error)
        }
       
    }

    addHorario() {
        const control = <FormArray>this.grupoHorarios.controls['horaInicioFin'];
        const posicion = control.value.length-1
        if (posicion>-1) {
            const horaInicio = control.at(posicion).get('horaInicio').value
            const horaFin = control.at(posicion).get('horaFin').value
            const fechaFin = new Date(control.at(posicion).get('horaFin').value)
            if (fechaFin.getHours() > 19) {
                return __messageSnackBar(this._matSnackBar,'Los horarios deben ser minimo 2 horas, el horario maximo de atención es 21:00')
            }
            const fechaAddInicio = fechaFin.setHours(fechaFin.getHours()+1)
            const fechaAddFin = fechaFin.setHours(fechaFin.getHours()+2)
        }
        // const horaInicio = control.at(posicion).get('horaInicio').value
        // const horaFin = control.at(posicion).get('horaFin').value
        const horaInicio = new Date(control.at(posicion).get('horaInicio').value)
        const horaFin = new Date(control.at(posicion).get('horaFin').value)
        const fechaAddInicio = horaFin.setHours(horaFin.getHours()+1)
            const fechaAddFin = horaFin.setHours(horaFin.getHours()+2)
        control.push(this.fb.group({ horaInicio: [fechaAddInicio,Validators.required], horaFin: [fechaAddFin,Validators.required],validate:[''] }))
    }

    removeTurno(index: number) {
        const control = <FormArray>this.grupoHorarios.controls['horaInicioFin'];
        control.removeAt(index)
    }

    validarTurnos():Boolean{
        const control = <FormArray>this.grupoHorarios.controls['horaInicioFin'];

        console.log("control:::"+control.value)
        var validacion = true;
        for (let index = 0; index < control.value.length; index++) {
            const horaInicio = control.at(index).get('horaInicio').value
            const horaFin = control.at(index).get('horaFin').value
            const valueValidacion = this.comparaHoras(new Date(horaInicio),new Date(horaFin))
            if (valueValidacion) {
                control.at(index).get('validate').setValue('✔️ |    ')
            } else {
                control.at(index).get('validate').setValue('      | ❌')
                validacion=false
            }
            //console.log("control.at(index)",control.at(index).get('validate').setValue('✔️ | ✔️'))
            // ✔️✔️❌
        }
        return validacion;

    }

    comparaHoras(time1:Date,time2:Date):Boolean{
        const minute1 = time1.getMinutes();
        const hour1 = time1.getHours();
        const minute2 = time2.getMinutes();
        const hour2 = time2.getHours();
        if (time1>=time2) return false;
        if(minute1%15 !=0) return false;
        if(minute2%15 !=0) return false;
        if( (hour2*60+minute2)-(hour1*60+minute1) <120) return false
        return true;
    }

}

export class Horarios {
    horaInicioFin: Horario[];
}
export class Horario {
    horaInicio: Date;
    horaFin: Date;
    validate:String;
}