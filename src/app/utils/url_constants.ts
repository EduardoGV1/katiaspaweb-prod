import { environment } from "src/environments/environment";

export const USUARIO_URL = `${environment.API_URL}Usuario/`

export const SERVICIO_URL = `${environment.API_URL}Servicio/`

export const CLIENTE_URL = `${environment.API_URL}Cliente/`

export const COSULTORIO_URL = `${environment.API_URL}Consultorio/`

export const CATEGORIASERVICIOS_URL =  `${environment.API_URL}Categoria/`

export const VENTAS_URL = `${environment.API_URL}Pagos/`

export const CITA_URL = `${environment.API_URL}Cita/`

export const ENCUESTA_URL = `${environment.API_URL}Encuesta/`
export const DASHBOARD_URL =`${environment.API_URL}Dashboard/`

