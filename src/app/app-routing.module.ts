import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { PagesComponent } from './pages/pages.component';
import { AtencionModule } from './pages/atencion/atencion.module';
import { RolComponent } from './pages/roles/rol/rol.component';

const routes: Routes = [
  { path: 'Auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
  { 
    path: 'Dashboard',
    component: PagesComponent,
    children: [
      { 
        path: "" ,
        component: DashboardComponent,
        data: { breadcrumb: "Dashboard" }
      }
    ],
  },
  
  { 
    path: 'Roles',
    loadChildren: () => import('./pages/roles/rol.module').then(m => m.RolModule),
  },
  { 
    path: 'Production',
    loadChildren: () => import('./pages/products/product.module').then(m => m.ProductModule),
  },
  { 
    path: 'Persons',
    loadChildren: () => import('./pages/persons/persons.module').then(m => m.PersonsModule),
  },
  { 
    path: 'Atencion',
    loadChildren: () => import('./pages/atencion/atencion.module').then(m => m.AtencionModule),
  },
  { 
    path: 'Sales',
    loadChildren: () => import('./pages/sales/ventas.module').then(m => m.VentasModule),
  },
  { 
    path: 'GestionCitas',
    loadChildren: () => import('./pages/gestioncitas/gestioncitas.module').then(m => m.GestionCitasModule),
  },
  { 
    path: 'CalidadAtencion',
    loadChildren: () => import('./pages/calidadatencion/calidadatencion.module').then(m => m.CalidadAtencionModule),
  },
  {
    path: '',
    loadChildren: () => import('./pages/citas/citas.module').then(m => m.CitasModule),
  },
  { path: '**', redirectTo: '/Auth/Login', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
