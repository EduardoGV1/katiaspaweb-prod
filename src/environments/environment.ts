// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // API_URL:"https://localhost:7046/api/",
  // API_URL:"https://ksapi20231222000459.azurewebsites.net/api/",
  //DEVELOP
  // API_URL:"https://joelasencios99-001-site2.atempurl.com/api/",
  //PROD
  API_URL:"https://joelasencios99-001-site1.atempurl.com/api/",
  //https://ks-api.conveyor.cloud/api
   //API_URL:"https://localhost:44337/api/",
   //API_URL:"https://katiaspa-api.conveyor.cloud/api/",
  //API_URL   : "https://katiaspaback.azurewebsites.net/api/",
  //API_URL:"https://katiaspa-api.conveyor.cloud/api/", 
  //API_URL:"https://ks-api.conveyor.cloud/api/",
  firebase  : {
    apiKey           : "AIzaSyAwUNfSoTWhfVk4KNViDf1JaeGu1D_Xbk0",
    authDomain       : "katia-spa.firebaseapp.com",
    databaseURL      : "https://katia-spa.firebaseio.com/",
    projectId        : "katia-spa",
    storageBucket    : "katia-spa.appspot.com",
    messagingSenderId: "583200739034",
    appId            : "1:583200739034:web:e0964b1ea2bb264a701fe3",
    measurementId    : "G-JF8H6E6147"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
